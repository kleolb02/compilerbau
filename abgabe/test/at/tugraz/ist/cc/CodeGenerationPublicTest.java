package at.tugraz.ist.cc;

import static org.junit.Assert.*;

import org.junit.Test;

import at.tugraz.ist.cc.interfaces.CodeGenerationInterface;

public class CodeGenerationPublicTest {

	CodeGenerationInterface codeGeneration = new CodeGeneration();
	
    @Test
    public void testFibo1() {
        int result = codeGeneration.createCode("test/testData/byteCode/fibo.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testFibo2() {
        int result = codeGeneration.createCode("test/testData/byteCode/fibo_2.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testPrint() {
        int result = codeGeneration.createCode("test/testData/byteCode/print.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testArithm() {
        int result = codeGeneration.createCode("test/testData/byteCode/arithm.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testArithmUn() {
        int result = codeGeneration.createCode("test/testData/byteCode/arithm_un.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testRelation() {
        int result = codeGeneration.createCode("test/testData/byteCode/relation.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testLogop() {
        int result = codeGeneration.createCode("test/testData/byteCode/logop.calc");
        assertEquals(0, result);        
    }
    
}
