package at.tugraz.ist.cc;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import at.tugraz.ist.cc.interfaces.IntermediateCodeInterface;

public class IntermediateCodePublicTest {

	IntermediateCodeInterface intermediate = new IntermediateCode();
	boolean debug = true;

	@Test
	public void test01Arithm() {
		assertEquals(0,	intermediate.createIntermediateCode("test/testData/intermediate/pub01_arithm.calc",debug, System.out));
	}
	
	@Test
	public void test02For() {
		assertEquals(0,	intermediate.createIntermediateCode("test/testData/intermediate/pub02_testFor.calc",debug, System.out));
	}
	
	@Test
	public void test03If() {
		assertEquals(0, intermediate.createIntermediateCode("test/testData/intermediate/pub03_testIf.calc",debug, System.out));
	}
	
	@Test
	public void test04GCDIf() {
		assertEquals(0,	intermediate.createIntermediateCode("test/testData/intermediate/pub04_gcdif.calc",debug, System.out));
	}

	@Test
	public void test05FunctionCalls() {
		assertEquals(0, intermediate.createIntermediateCode("test/testData/intermediate/pub05_FunctionCall.calc",debug, System.out));
	}
	
}
