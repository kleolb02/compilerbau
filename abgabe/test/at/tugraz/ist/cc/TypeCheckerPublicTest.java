package at.tugraz.ist.cc;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import at.tugraz.ist.cc.interfaces.TypeCheckerInterface;

@Ignore
public class TypeCheckerPublicTest {

	TypeCheckerInterface typeChecker = new TypeChecker();
	boolean debug = true;
	
    @Test
    public void testPass01() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass01.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass02() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass02.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass03() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass03.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass04() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass04.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass05() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass05.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass06() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass06.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass07() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass07.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass08() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass08.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass09() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass09.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass10() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass10.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testPass11() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/pass11.calc", debug);
        assertEquals(0, result);        
    }
    
    @Test
    public void testFail01() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail01.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail02() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail02.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail03() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail03.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail04() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail04.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail05() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail05.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail06() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail06.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail07() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail07.calc", debug);
        assertEquals(1, result);        
    }
    
    @Test
    public void testFail08() {
        int result = typeChecker.checkTypes("test/testData/typeChecking/fail08.calc", debug);
        assertEquals(1, result);        
    }
    
}
