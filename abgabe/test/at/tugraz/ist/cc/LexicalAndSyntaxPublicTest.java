package at.tugraz.ist.cc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.junit.Ignore;
import org.junit.Test;

import at.tugraz.ist.cc.interfaces.LexicalAndSyntaxAnalyzerInterface;

public class LexicalAndSyntaxPublicTest {

	LexicalAndSyntaxAnalyzerInterface lexAndSyntax = new LexicalAndSyntaxAnalyzer();
	boolean debug = true;
    boolean debugDebug = true;
		
    @Test
    public void testLexerPass01() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/pass01_declarations.calc", debug);
        if(debugDebug && 0 != result)
        System.out.print("FAIL IN: pass01_declarations.calc");
    	assertEquals(0,result);
    }
	
    @Test
    public void testLexerPass02() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/pass02_statement.calc", debug);
        if(debugDebug && 0 != result)
            System.out.print("FAIL IN: pass02_statement.calc");
    	assertEquals(0,result);    
    }
    
    @Test
    public void testLexerPass03() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/pass03_functhead.calc", debug);
        if(debugDebug && 0 != result)
            System.out.print("FAIL IN: pass03_functhead.calc");
    	assertEquals(0,result);    
    }
    
    @Test
    public void testLexerPass04() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/pass04_gcd.calc", debug);
        if(debugDebug && 0 != result)
            System.out.print("FAIL IN: pass04_gcd.calc");
    	assertEquals(0,result);    
    }

    @Test
    public void testLexerFail01() {
        // input: 12asdfb, 2asdf, _adf
    	int result = lexAndSyntax.lexer("test/testData/lexer/fail01_idlist.calc", debug);
        if(debugDebug && 1 != result)
            System.out.print("FAIL IN: fail01_idlist.calc " + result + "-");
    	assertEquals(1,result);    
    }
    
    @Test
    public void testLexerFail02() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/fail02_id.calc", debug);
        if(debugDebug && result < 4)
            System.out.print("FAIL IN: fail02_id.calc");
    	assertTrue(result >= 4);    
    }
	
    @Test
    public void testLexerFail03() {
    	int result = lexAndSyntax.lexer("test/testData/lexer/fail03_special.calc", debug);
        if(debugDebug && result < 2)
            System.out.print("FAIL IN: fail03_special.calc");
    	assertTrue(result >= 2);    
    }
    
    @Test
    public void testParserPass01() {
        int result = lexAndSyntax.checkSyntax("test/testData/parser/pass01_arithm.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass01_arithm.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testParserPass02() {
        int result = lexAndSyntax.checkSyntax("test/testData/parser/pass02_comp.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass02_comp.calc");
        assertEquals(0, result);        
    }

    @Test
    public void testParserPass03() {
        int result = lexAndSyntax.checkSyntax("test/testData/parser/pass03_decl.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass03_decl.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testParserPass04() {
        int result = lexAndSyntax.checkSyntax("test/testData/parser/pass04_gcdif.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass04_gcdif.calc");
        assertEquals(0, result);        
    }
    
    @Test
    public void testParserPass05() {
        int result = lexAndSyntax.checkSyntax("test/testData/parser/pass05_for.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass05_for.calc");
        assertEquals(0, result);
    }

	@Test
	public void testParserPass06() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/pass06_funct.calc", debug);
        if(debugDebug && result > 0)
            System.out.print("FAIL IN: pass06_funct.calc");
	    assertEquals(0, result);        
	}
    
	@Test
	public void testParserFail01() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/fail01_assign.calc", debug);
        if(debugDebug && result != 2)
            System.out.print("FAIL IN: fail01_assign.calc");
	    assertEquals(2, result);        
	}
	
	@Test
	public void testParserFail02() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/fail02_for.calc", debug);
        if(debugDebug && result < 3)
            System.out.print("FAIL IN: fail02_for.calc");
	    assertTrue(result >= 3);        
	}

	@Test
	public void testParserFail03() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/fail03_decl.calc", debug);
        if(debugDebug && result < 1)
            System.out.print("FAIL IN: fail03_decl.calc");
	    assertTrue(result >= 1);        
	}
	
	@Test
	public void testParserFail04() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/fail04_arithm.calc", debug);
        if(debugDebug && result != 4)
            System.out.print("FAIL IN: fail04_arithm.calc");
	    assertEquals(4, result);        
	}
	
	@Test
	public void testParserFail05() {
	    int result = lexAndSyntax.checkSyntax("test/testData/parser/fail05_leading0.calc", debug);
        if(debugDebug && result != 2)
            System.out.print("FAIL IN: fail05_leading0.calc");
	    assertEquals(2, result);
	}

    @Test
    @Ignore
    public void floatValues() throws Exception {
        CommonTree root = lexAndSyntax.getTree("test/testData/own/parser01_floatValues.calc", debug);
        Tree tree = root.getChild(0);

        Tree intToken = tree.getChild(6);
        assertEquals("INT", getTokenType(intToken));
        assertEquals("2", intToken.getText());

        Tree floatToken1 = tree.getChild(8);
        assertEquals("FLOAT", getTokenType(floatToken1));
        assertEquals("5.123", floatToken1.getText());

        Tree floatToken2 = tree.getChild(10);
        assertEquals("FLOAT", getTokenType(floatToken2));
        assertEquals("0.1", floatToken2.getText());
    }

    @Test
    @Ignore
    public void singleLineComment() throws Exception {
        CommonTree tree = lexAndSyntax.getTree("test/testData/own/parser02_singleLineComment.calc", debug);
        if(debugDebug && tree.getChildCount() != 9)
            System.out.print("FAIL IN: parser02_singleLineComment.calc");
        assertEquals(9, tree.getChildCount());
    }

    @Test
    @Ignore
    public void multiLineComment() throws Exception {
        CommonTree tree = lexAndSyntax.getTree("test/testData/own/parser03_multiLineComment.calc", debug);
        if(debugDebug && tree.getChildCount() != 9)
            System.out.print("FAIL IN: parser03_multiLineComment.calc");
        assertEquals(9, tree.getChildCount());
    }

    @Test
    public void doWhileLoop() {
        int result = lexAndSyntax.checkSyntax("test/testData/own/parser04_doWhileLoop.calc", debug);
        if(debugDebug && result != 0)
            System.out.print("FAIL IN: parser04_doWhileLoop.calc");
        assertEquals(0, result);
    }

    @Test
    public void whileLoop() {
        int result = lexAndSyntax.checkSyntax("test/testData/own/parser05_whileLoop.calc", debug);
        if(debugDebug && result != 0)
            System.out.print("FAIL IN: parser05_whileLoop.calc");
        assertEquals(0, result);
    }


    private String getTokenType(Tree child) {
        return CalculatorParser.tokenNames[child.getType()];
    }
}
