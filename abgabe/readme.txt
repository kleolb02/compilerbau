GroupXX - Task 1

Oliver Klemencic    (0930001)   oliver.klemencic@student.tugraz.at
Miriam Mustermann   (0930002)   miriam.mustermann@student.tugraz.at
John Doe            (0930003)   john.doe@student.tugraz.at
Jane Doe            (0930010)   j.doe@student.tugraz.at


Description of the Program:
---------------------------
A lexical- and syntax-analyzer was implemented. Supplied with an input file, the Analyzer checks 
whether the program is lexically and syntactically correct. Otherwise, ...

Usage: java at.tugraz.ist.cc.LexicalAndSyntaxAnalyzer.lexer <path_to_input_file> <debug>
	   java at.tugraz.ist.cc.LexicalAndSyntaxAnalyzer.checkSyntax <path_to_input_file> <debug>


-------------------
Remarks for task 1:
-------------------
General remarks:
----------------
- ...

Changes  made:
--------------
- Adapted the definition of strings to match anything within double quotes


Known limitations:
------------------
- Currently no known issues

Implemented BONUS tasks:
------------------------
- Added support for single-line comments: // ... (see unit test 'LexicalAndSyntaxPublicTest.singleLineComment')
- Added support for multi-line comments: /* ... */ (see unit test 'LexicalAndSyntaxPublicTest.multiLineComment')
- Added support for do..while loops (see unit test 'LexicalAndSyntaxPublicTest.doWhileLoop')
- Added support for while loops (see unit test 'LexicalAndSyntaxPublicTest.whileLoop')
- Added support for floats (using the '.' as separator) and the keyword 'float' as type name (see unit test 'LexicalAndSyntaxPublicTest.floatValues')

Percentage of participation:
----------------------------
Oliver Klemencic       20 %
Miriam Mustermann      25 %
John Doe               25 %
Jane Doe               30 %


-------------------
Remarks for task 2:
-------------------
General remarks:
----------------
...

Changes  made:
--------------
...

Known limitations:
------------------
...

Implemented BONUS tasks:
------------------------
...

Percentage of participation:
----------------------------
...


-------------------
Remarks for task 3:
-------------------
...


-------------------
Remarks for task 4:
-------------------
...