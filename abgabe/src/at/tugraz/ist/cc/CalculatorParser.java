// $ANTLR 3.5 src/Calculator.g 2014-03-28 21:32:14

package at.tugraz.ist.cc;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class CalculatorParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "ANDOP", "ARGS", "ASSIGNMENT", 
		"ASSIGNMENT_OR_METHOD_CALL", "ASSIGNOP", "BLOCK", "BOOL", "CASE", "DIGIT", 
		"DIGIT0", "DOWHILE", "DT_BOOLEAN", "DT_FLOAT", "DT_INT", "DT_STRING", 
		"ESC_SEQ", "EXPRESSION", "FLOAT", "FOR", "ID", "IF", "INT", "LCURL", "LETTER", 
		"METHOD_BODY", "METHOD_CALL", "METHOD_DECL", "MULTIPLYOP", "MULTI_LINE_COMMENT", 
		"NOT", "OR", "OROP", "PARAMETER", "PARAMS", "PLUSMINUS", "PROGRAM", "RCURL", 
		"RELOP", "RETURN", "SINGLE_LINE_COMMENT", "STRING", "VARIABLE", "VARIABLE_DEF", 
		"WHILE", "WS", "'('", "')'", "','", "';'", "'break'", "'continue'", "'do'", 
		"'else'", "'return'", "'while'"
	};
	public static final int EOF=-1;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int AND=4;
	public static final int ANDOP=5;
	public static final int ARGS=6;
	public static final int ASSIGNMENT=7;
	public static final int ASSIGNMENT_OR_METHOD_CALL=8;
	public static final int ASSIGNOP=9;
	public static final int BLOCK=10;
	public static final int BOOL=11;
	public static final int CASE=12;
	public static final int DIGIT=13;
	public static final int DIGIT0=14;
	public static final int DOWHILE=15;
	public static final int DT_BOOLEAN=16;
	public static final int DT_FLOAT=17;
	public static final int DT_INT=18;
	public static final int DT_STRING=19;
	public static final int ESC_SEQ=20;
	public static final int EXPRESSION=21;
	public static final int FLOAT=22;
	public static final int FOR=23;
	public static final int ID=24;
	public static final int IF=25;
	public static final int INT=26;
	public static final int LCURL=27;
	public static final int LETTER=28;
	public static final int METHOD_BODY=29;
	public static final int METHOD_CALL=30;
	public static final int METHOD_DECL=31;
	public static final int MULTIPLYOP=32;
	public static final int MULTI_LINE_COMMENT=33;
	public static final int NOT=34;
	public static final int OR=35;
	public static final int OROP=36;
	public static final int PARAMETER=37;
	public static final int PARAMS=38;
	public static final int PLUSMINUS=39;
	public static final int PROGRAM=40;
	public static final int RCURL=41;
	public static final int RELOP=42;
	public static final int RETURN=43;
	public static final int SINGLE_LINE_COMMENT=44;
	public static final int STRING=45;
	public static final int VARIABLE=46;
	public static final int VARIABLE_DEF=47;
	public static final int WHILE=48;
	public static final int WS=49;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public CalculatorParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public CalculatorParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	protected TreeAdaptor adaptor = new CommonTreeAdaptor();

	public void setTreeAdaptor(TreeAdaptor adaptor) {
		this.adaptor = adaptor;
	}
	public TreeAdaptor getTreeAdaptor() {
		return adaptor;
	}
	@Override public String[] getTokenNames() { return CalculatorParser.tokenNames; }
	@Override public String getGrammarFileName() { return "src/Calculator.g"; }


	private List<RecognitionException> errors = new ArrayList<RecognitionException>();

	public List<RecognitionException> getAllErrors() {
		return errors;
	}

	//override method

	@Override
	public void reportError(RecognitionException e) {
		errors.add(e);
	}



	public static class program_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "program"
	// src/Calculator.g:149:1: program : methodDeclarations -> ^( PROGRAM methodDeclarations ) ;
	public final CalculatorParser.program_return program() throws RecognitionException {
		CalculatorParser.program_return retval = new CalculatorParser.program_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope methodDeclarations1 =null;

		RewriteRuleSubtreeStream stream_methodDeclarations=new RewriteRuleSubtreeStream(adaptor,"rule methodDeclarations");

		try {
			// src/Calculator.g:149:9: ( methodDeclarations -> ^( PROGRAM methodDeclarations ) )
			// src/Calculator.g:150:2: methodDeclarations
			{
			pushFollow(FOLLOW_methodDeclarations_in_program756);
			methodDeclarations1=methodDeclarations();
			state._fsp--;

			stream_methodDeclarations.add(methodDeclarations1.getTree());
			// AST REWRITE
			// elements: methodDeclarations
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 150:21: -> ^( PROGRAM methodDeclarations )
			{
				// src/Calculator.g:150:24: ^( PROGRAM methodDeclarations )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PROGRAM, "PROGRAM"), root_1);
				adaptor.addChild(root_1, stream_methodDeclarations.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "program"


	public static class methodDeclarations_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodDeclarations"
	// src/Calculator.g:153:1: methodDeclarations : ( methodDeclaration )+ ;
	public final CalculatorParser.methodDeclarations_return methodDeclarations() throws RecognitionException {
		CalculatorParser.methodDeclarations_return retval = new CalculatorParser.methodDeclarations_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope methodDeclaration2 =null;


		try {
			// src/Calculator.g:154:2: ( ( methodDeclaration )+ )
			// src/Calculator.g:155:2: ( methodDeclaration )+
			{
			root_0 = (CommonTree)adaptor.nil();


			// src/Calculator.g:155:2: ( methodDeclaration )+
			int cnt1=0;
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= DT_BOOLEAN && LA1_0 <= DT_STRING)) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// src/Calculator.g:155:3: methodDeclaration
					{
					pushFollow(FOLLOW_methodDeclaration_in_methodDeclarations776);
					methodDeclaration2=methodDeclaration();
					state._fsp--;

					adaptor.addChild(root_0, methodDeclaration2.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					throw eee;
				}
				cnt1++;
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodDeclarations"


	public static class methodDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodDeclaration"
	// src/Calculator.g:158:1: methodDeclaration : methodHeader methodBody -> ^( METHOD_DECL methodHeader methodBody ) ;
	public final CalculatorParser.methodDeclaration_return methodDeclaration() throws RecognitionException {
		CalculatorParser.methodDeclaration_return retval = new CalculatorParser.methodDeclaration_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope methodHeader3 =null;
		ParserRuleReturnScope methodBody4 =null;

		RewriteRuleSubtreeStream stream_methodHeader=new RewriteRuleSubtreeStream(adaptor,"rule methodHeader");
		RewriteRuleSubtreeStream stream_methodBody=new RewriteRuleSubtreeStream(adaptor,"rule methodBody");

		try {
			// src/Calculator.g:159:2: ( methodHeader methodBody -> ^( METHOD_DECL methodHeader methodBody ) )
			// src/Calculator.g:160:5: methodHeader methodBody
			{
			pushFollow(FOLLOW_methodHeader_in_methodDeclaration792);
			methodHeader3=methodHeader();
			state._fsp--;

			stream_methodHeader.add(methodHeader3.getTree());
			pushFollow(FOLLOW_methodBody_in_methodDeclaration798);
			methodBody4=methodBody();
			state._fsp--;

			stream_methodBody.add(methodBody4.getTree());
			// AST REWRITE
			// elements: methodBody, methodHeader
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 161:16: -> ^( METHOD_DECL methodHeader methodBody )
			{
				// src/Calculator.g:161:19: ^( METHOD_DECL methodHeader methodBody )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(METHOD_DECL, "METHOD_DECL"), root_1);
				adaptor.addChild(root_1, stream_methodHeader.nextTree());
				adaptor.addChild(root_1, stream_methodBody.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodDeclaration"


	public static class methodHeader_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodHeader"
	// src/Calculator.g:164:1: methodHeader : primitiveType ID formalParameterList ;
	public final CalculatorParser.methodHeader_return methodHeader() throws RecognitionException {
		CalculatorParser.methodHeader_return retval = new CalculatorParser.methodHeader_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID6=null;
		ParserRuleReturnScope primitiveType5 =null;
		ParserRuleReturnScope formalParameterList7 =null;

		CommonTree ID6_tree=null;

		try {
			// src/Calculator.g:165:5: ( primitiveType ID formalParameterList )
			// src/Calculator.g:166:5: primitiveType ID formalParameterList
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_primitiveType_in_methodHeader826);
			primitiveType5=primitiveType();
			state._fsp--;

			adaptor.addChild(root_0, primitiveType5.getTree());

			ID6=(Token)match(input,ID,FOLLOW_ID_in_methodHeader832); 
			ID6_tree = (CommonTree)adaptor.create(ID6);
			adaptor.addChild(root_0, ID6_tree);

			pushFollow(FOLLOW_formalParameterList_in_methodHeader838);
			formalParameterList7=formalParameterList();
			state._fsp--;

			adaptor.addChild(root_0, formalParameterList7.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodHeader"


	public static class methodBody_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "methodBody"
	// src/Calculator.g:171:1: methodBody : LCURL ( blockStatement )* returnStatement RCURL -> ^( METHOD_BODY ( blockStatement )* returnStatement RCURL ) ;
	public final CalculatorParser.methodBody_return methodBody() throws RecognitionException {
		CalculatorParser.methodBody_return retval = new CalculatorParser.methodBody_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token LCURL8=null;
		Token RCURL11=null;
		ParserRuleReturnScope blockStatement9 =null;
		ParserRuleReturnScope returnStatement10 =null;

		CommonTree LCURL8_tree=null;
		CommonTree RCURL11_tree=null;
		RewriteRuleTokenStream stream_LCURL=new RewriteRuleTokenStream(adaptor,"token LCURL");
		RewriteRuleTokenStream stream_RCURL=new RewriteRuleTokenStream(adaptor,"token RCURL");
		RewriteRuleSubtreeStream stream_returnStatement=new RewriteRuleSubtreeStream(adaptor,"rule returnStatement");
		RewriteRuleSubtreeStream stream_blockStatement=new RewriteRuleSubtreeStream(adaptor,"rule blockStatement");

		try {
			// src/Calculator.g:172:5: ( LCURL ( blockStatement )* returnStatement RCURL -> ^( METHOD_BODY ( blockStatement )* returnStatement RCURL ) )
			// src/Calculator.g:173:6: LCURL ( blockStatement )* returnStatement RCURL
			{
			LCURL8=(Token)match(input,LCURL,FOLLOW_LCURL_in_methodBody860);  
			stream_LCURL.add(LCURL8);

			// src/Calculator.g:174:6: ( blockStatement )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= DT_BOOLEAN && LA2_0 <= DT_STRING)||(LA2_0 >= FOR && LA2_0 <= IF)||LA2_0==LCURL||(LA2_0 >= 54 && LA2_0 <= 56)||LA2_0==59) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// src/Calculator.g:174:7: blockStatement
					{
					pushFollow(FOLLOW_blockStatement_in_methodBody868);
					blockStatement9=blockStatement();
					state._fsp--;

					stream_blockStatement.add(blockStatement9.getTree());
					}
					break;

				default :
					break loop2;
				}
			}

			pushFollow(FOLLOW_returnStatement_in_methodBody877);
			returnStatement10=returnStatement();
			state._fsp--;

			stream_returnStatement.add(returnStatement10.getTree());
			RCURL11=(Token)match(input,RCURL,FOLLOW_RCURL_in_methodBody884);  
			stream_RCURL.add(RCURL11);

			// AST REWRITE
			// elements: RCURL, blockStatement, returnStatement
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 177:6: -> ^( METHOD_BODY ( blockStatement )* returnStatement RCURL )
			{
				// src/Calculator.g:177:9: ^( METHOD_BODY ( blockStatement )* returnStatement RCURL )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(METHOD_BODY, "METHOD_BODY"), root_1);
				// src/Calculator.g:177:23: ( blockStatement )*
				while ( stream_blockStatement.hasNext() ) {
					adaptor.addChild(root_1, stream_blockStatement.nextTree());
				}
				stream_blockStatement.reset();

				adaptor.addChild(root_1, stream_returnStatement.nextTree());
				adaptor.addChild(root_1, stream_RCURL.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "methodBody"


	public static class returnStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "returnStatement"
	// src/Calculator.g:180:4: returnStatement : 'return' expression ';' -> ^( RETURN expression ) ;
	public final CalculatorParser.returnStatement_return returnStatement() throws RecognitionException {
		CalculatorParser.returnStatement_return retval = new CalculatorParser.returnStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal12=null;
		Token char_literal14=null;
		ParserRuleReturnScope expression13 =null;

		CommonTree string_literal12_tree=null;
		CommonTree char_literal14_tree=null;
		RewriteRuleTokenStream stream_58=new RewriteRuleTokenStream(adaptor,"token 58");
		RewriteRuleTokenStream stream_53=new RewriteRuleTokenStream(adaptor,"token 53");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// src/Calculator.g:181:1: ( 'return' expression ';' -> ^( RETURN expression ) )
			// src/Calculator.g:182:1: 'return' expression ';'
			{
			string_literal12=(Token)match(input,58,FOLLOW_58_in_returnStatement925);  
			stream_58.add(string_literal12);

			pushFollow(FOLLOW_expression_in_returnStatement927);
			expression13=expression();
			state._fsp--;

			stream_expression.add(expression13.getTree());
			char_literal14=(Token)match(input,53,FOLLOW_53_in_returnStatement929);  
			stream_53.add(char_literal14);

			// AST REWRITE
			// elements: expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 183:4: -> ^( RETURN expression )
			{
				// src/Calculator.g:183:4: ^( RETURN expression )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(RETURN, "RETURN"), root_1);
				adaptor.addChild(root_1, stream_expression.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "returnStatement"


	public static class blockStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "blockStatement"
	// src/Calculator.g:186:1: blockStatement : ( localVariableDeclarationStatement | statement );
	public final CalculatorParser.blockStatement_return blockStatement() throws RecognitionException {
		CalculatorParser.blockStatement_return retval = new CalculatorParser.blockStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope localVariableDeclarationStatement15 =null;
		ParserRuleReturnScope statement16 =null;


		try {
			// src/Calculator.g:187:5: ( localVariableDeclarationStatement | statement )
			int alt3=2;
			int LA3_0 = input.LA(1);
			if ( ((LA3_0 >= DT_BOOLEAN && LA3_0 <= DT_STRING)) ) {
				alt3=1;
			}
			else if ( ((LA3_0 >= FOR && LA3_0 <= IF)||LA3_0==LCURL||(LA3_0 >= 54 && LA3_0 <= 56)||LA3_0==59) ) {
				alt3=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				throw nvae;
			}

			switch (alt3) {
				case 1 :
					// src/Calculator.g:187:9: localVariableDeclarationStatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_localVariableDeclarationStatement_in_blockStatement953);
					localVariableDeclarationStatement15=localVariableDeclarationStatement();
					state._fsp--;

					adaptor.addChild(root_0, localVariableDeclarationStatement15.getTree());

					}
					break;
				case 2 :
					// src/Calculator.g:188:9: statement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_statement_in_blockStatement963);
					statement16=statement();
					state._fsp--;

					adaptor.addChild(root_0, statement16.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "blockStatement"


	public static class statement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "statement"
	// src/Calculator.g:191:1: statement : ( ifStatement | forStatement | block | whileStatement | 'do' ^ statement 'while' ! paranthesisExpression ';' !| 'break' ^ ';' !| 'continue' ^ ';' !| ( assignmentOrMethodCallStatement ';' !) );
	public final CalculatorParser.statement_return statement() throws RecognitionException {
		CalculatorParser.statement_return retval = new CalculatorParser.statement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal21=null;
		Token string_literal23=null;
		Token char_literal25=null;
		Token string_literal26=null;
		Token char_literal27=null;
		Token string_literal28=null;
		Token char_literal29=null;
		Token char_literal31=null;
		ParserRuleReturnScope ifStatement17 =null;
		ParserRuleReturnScope forStatement18 =null;
		ParserRuleReturnScope block19 =null;
		ParserRuleReturnScope whileStatement20 =null;
		ParserRuleReturnScope statement22 =null;
		ParserRuleReturnScope paranthesisExpression24 =null;
		ParserRuleReturnScope assignmentOrMethodCallStatement30 =null;

		CommonTree string_literal21_tree=null;
		CommonTree string_literal23_tree=null;
		CommonTree char_literal25_tree=null;
		CommonTree string_literal26_tree=null;
		CommonTree char_literal27_tree=null;
		CommonTree string_literal28_tree=null;
		CommonTree char_literal29_tree=null;
		CommonTree char_literal31_tree=null;

		try {
			// src/Calculator.g:192:2: ( ifStatement | forStatement | block | whileStatement | 'do' ^ statement 'while' ! paranthesisExpression ';' !| 'break' ^ ';' !| 'continue' ^ ';' !| ( assignmentOrMethodCallStatement ';' !) )
			int alt4=8;
			switch ( input.LA(1) ) {
			case IF:
				{
				alt4=1;
				}
				break;
			case FOR:
				{
				alt4=2;
				}
				break;
			case LCURL:
				{
				alt4=3;
				}
				break;
			case 59:
				{
				alt4=4;
				}
				break;
			case 56:
				{
				alt4=5;
				}
				break;
			case 54:
				{
				alt4=6;
				}
				break;
			case 55:
				{
				alt4=7;
				}
				break;
			case ID:
				{
				alt4=8;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}
			switch (alt4) {
				case 1 :
					// src/Calculator.g:192:4: ifStatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_ifStatement_in_statement977);
					ifStatement17=ifStatement();
					state._fsp--;

					adaptor.addChild(root_0, ifStatement17.getTree());

					}
					break;
				case 2 :
					// src/Calculator.g:193:4: forStatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_forStatement_in_statement982);
					forStatement18=forStatement();
					state._fsp--;

					adaptor.addChild(root_0, forStatement18.getTree());

					}
					break;
				case 3 :
					// src/Calculator.g:194:4: block
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_block_in_statement987);
					block19=block();
					state._fsp--;

					adaptor.addChild(root_0, block19.getTree());

					}
					break;
				case 4 :
					// src/Calculator.g:195:4: whileStatement
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_whileStatement_in_statement992);
					whileStatement20=whileStatement();
					state._fsp--;

					adaptor.addChild(root_0, whileStatement20.getTree());

					}
					break;
				case 5 :
					// src/Calculator.g:196:8: 'do' ^ statement 'while' ! paranthesisExpression ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal21=(Token)match(input,56,FOLLOW_56_in_statement1001); 
					string_literal21_tree = (CommonTree)adaptor.create(string_literal21);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal21_tree, root_0);

					pushFollow(FOLLOW_statement_in_statement1004);
					statement22=statement();
					state._fsp--;

					adaptor.addChild(root_0, statement22.getTree());

					string_literal23=(Token)match(input,59,FOLLOW_59_in_statement1006); 
					pushFollow(FOLLOW_paranthesisExpression_in_statement1009);
					paranthesisExpression24=paranthesisExpression();
					state._fsp--;

					adaptor.addChild(root_0, paranthesisExpression24.getTree());

					char_literal25=(Token)match(input,53,FOLLOW_53_in_statement1011); 
					}
					break;
				case 6 :
					// src/Calculator.g:197:8: 'break' ^ ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal26=(Token)match(input,54,FOLLOW_54_in_statement1021); 
					string_literal26_tree = (CommonTree)adaptor.create(string_literal26);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal26_tree, root_0);

					char_literal27=(Token)match(input,53,FOLLOW_53_in_statement1024); 
					}
					break;
				case 7 :
					// src/Calculator.g:198:8: 'continue' ^ ';' !
					{
					root_0 = (CommonTree)adaptor.nil();


					string_literal28=(Token)match(input,55,FOLLOW_55_in_statement1034); 
					string_literal28_tree = (CommonTree)adaptor.create(string_literal28);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal28_tree, root_0);

					char_literal29=(Token)match(input,53,FOLLOW_53_in_statement1037); 
					}
					break;
				case 8 :
					// src/Calculator.g:199:8: ( assignmentOrMethodCallStatement ';' !)
					{
					root_0 = (CommonTree)adaptor.nil();


					// src/Calculator.g:199:8: ( assignmentOrMethodCallStatement ';' !)
					// src/Calculator.g:199:9: assignmentOrMethodCallStatement ';' !
					{
					pushFollow(FOLLOW_assignmentOrMethodCallStatement_in_statement1048);
					assignmentOrMethodCallStatement30=assignmentOrMethodCallStatement();
					state._fsp--;

					adaptor.addChild(root_0, assignmentOrMethodCallStatement30.getTree());

					char_literal31=(Token)match(input,53,FOLLOW_53_in_statement1050); 
					}

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "statement"


	public static class whileStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "whileStatement"
	// src/Calculator.g:202:1: whileStatement : 'while' paranthesisExpression statement -> ^( WHILE paranthesisExpression statement ) ;
	public final CalculatorParser.whileStatement_return whileStatement() throws RecognitionException {
		CalculatorParser.whileStatement_return retval = new CalculatorParser.whileStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal32=null;
		ParserRuleReturnScope paranthesisExpression33 =null;
		ParserRuleReturnScope statement34 =null;

		CommonTree string_literal32_tree=null;
		RewriteRuleTokenStream stream_59=new RewriteRuleTokenStream(adaptor,"token 59");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
		RewriteRuleSubtreeStream stream_paranthesisExpression=new RewriteRuleSubtreeStream(adaptor,"rule paranthesisExpression");

		try {
			// src/Calculator.g:203:2: ( 'while' paranthesisExpression statement -> ^( WHILE paranthesisExpression statement ) )
			// src/Calculator.g:204:2: 'while' paranthesisExpression statement
			{
			string_literal32=(Token)match(input,59,FOLLOW_59_in_whileStatement1064);  
			stream_59.add(string_literal32);

			pushFollow(FOLLOW_paranthesisExpression_in_whileStatement1066);
			paranthesisExpression33=paranthesisExpression();
			state._fsp--;

			stream_paranthesisExpression.add(paranthesisExpression33.getTree());
			pushFollow(FOLLOW_statement_in_whileStatement1068);
			statement34=statement();
			state._fsp--;

			stream_statement.add(statement34.getTree());
			// AST REWRITE
			// elements: statement, paranthesisExpression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 205:2: -> ^( WHILE paranthesisExpression statement )
			{
				// src/Calculator.g:205:5: ^( WHILE paranthesisExpression statement )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(WHILE, "WHILE"), root_1);
				adaptor.addChild(root_1, stream_paranthesisExpression.nextTree());
				adaptor.addChild(root_1, stream_statement.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "whileStatement"


	public static class localVariableDeclarationStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "localVariableDeclarationStatement"
	// src/Calculator.g:209:1: localVariableDeclarationStatement : localVariableDeclaration ^ ';' !;
	public final CalculatorParser.localVariableDeclarationStatement_return localVariableDeclarationStatement() throws RecognitionException {
		CalculatorParser.localVariableDeclarationStatement_return retval = new CalculatorParser.localVariableDeclarationStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal36=null;
		ParserRuleReturnScope localVariableDeclaration35 =null;

		CommonTree char_literal36_tree=null;

		try {
			// src/Calculator.g:210:5: ( localVariableDeclaration ^ ';' !)
			// src/Calculator.g:210:9: localVariableDeclaration ^ ';' !
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_localVariableDeclaration_in_localVariableDeclarationStatement1096);
			localVariableDeclaration35=localVariableDeclaration();
			state._fsp--;

			root_0 = (CommonTree)adaptor.becomeRoot(localVariableDeclaration35.getTree(), root_0);
			char_literal36=(Token)match(input,53,FOLLOW_53_in_localVariableDeclarationStatement1107); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "localVariableDeclarationStatement"


	public static class ifStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "ifStatement"
	// src/Calculator.g:214:1: ifStatement : IF paranthesisExpression statement ( options {greedy=true; } : elseStatement )? -> ^( IF paranthesisExpression statement ( elseStatement )? ) ;
	public final CalculatorParser.ifStatement_return ifStatement() throws RecognitionException {
		CalculatorParser.ifStatement_return retval = new CalculatorParser.ifStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token IF37=null;
		ParserRuleReturnScope paranthesisExpression38 =null;
		ParserRuleReturnScope statement39 =null;
		ParserRuleReturnScope elseStatement40 =null;

		CommonTree IF37_tree=null;
		RewriteRuleTokenStream stream_IF=new RewriteRuleTokenStream(adaptor,"token IF");
		RewriteRuleSubtreeStream stream_statement=new RewriteRuleSubtreeStream(adaptor,"rule statement");
		RewriteRuleSubtreeStream stream_paranthesisExpression=new RewriteRuleSubtreeStream(adaptor,"rule paranthesisExpression");
		RewriteRuleSubtreeStream stream_elseStatement=new RewriteRuleSubtreeStream(adaptor,"rule elseStatement");

		try {
			// src/Calculator.g:215:2: ( IF paranthesisExpression statement ( options {greedy=true; } : elseStatement )? -> ^( IF paranthesisExpression statement ( elseStatement )? ) )
			// src/Calculator.g:215:4: IF paranthesisExpression statement ( options {greedy=true; } : elseStatement )?
			{
			IF37=(Token)match(input,IF,FOLLOW_IF_in_ifStatement1122);  
			stream_IF.add(IF37);

			pushFollow(FOLLOW_paranthesisExpression_in_ifStatement1124);
			paranthesisExpression38=paranthesisExpression();
			state._fsp--;

			stream_paranthesisExpression.add(paranthesisExpression38.getTree());
			pushFollow(FOLLOW_statement_in_ifStatement1126);
			statement39=statement();
			state._fsp--;

			stream_statement.add(statement39.getTree());
			// src/Calculator.g:216:2: ( options {greedy=true; } : elseStatement )?
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0==57) ) {
				alt5=1;
			}
			switch (alt5) {
				case 1 :
					// src/Calculator.g:216:29: elseStatement
					{
					pushFollow(FOLLOW_elseStatement_in_ifStatement1140);
					elseStatement40=elseStatement();
					state._fsp--;

					stream_elseStatement.add(elseStatement40.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: elseStatement, paranthesisExpression, statement, IF
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 217:2: -> ^( IF paranthesisExpression statement ( elseStatement )? )
			{
				// src/Calculator.g:217:5: ^( IF paranthesisExpression statement ( elseStatement )? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot(stream_IF.nextNode(), root_1);
				adaptor.addChild(root_1, stream_paranthesisExpression.nextTree());
				adaptor.addChild(root_1, stream_statement.nextTree());
				// src/Calculator.g:217:42: ( elseStatement )?
				if ( stream_elseStatement.hasNext() ) {
					adaptor.addChild(root_1, stream_elseStatement.nextTree());
				}
				stream_elseStatement.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "ifStatement"


	public static class elseStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "elseStatement"
	// src/Calculator.g:220:1: elseStatement : 'else' ! statement ^;
	public final CalculatorParser.elseStatement_return elseStatement() throws RecognitionException {
		CalculatorParser.elseStatement_return retval = new CalculatorParser.elseStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal41=null;
		ParserRuleReturnScope statement42 =null;

		CommonTree string_literal41_tree=null;

		try {
			// src/Calculator.g:221:2: ( 'else' ! statement ^)
			// src/Calculator.g:221:4: 'else' ! statement ^
			{
			root_0 = (CommonTree)adaptor.nil();


			string_literal41=(Token)match(input,57,FOLLOW_57_in_elseStatement1167); 
			pushFollow(FOLLOW_statement_in_elseStatement1170);
			statement42=statement();
			state._fsp--;

			root_0 = (CommonTree)adaptor.becomeRoot(statement42.getTree(), root_0);
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "elseStatement"


	public static class forStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "forStatement"
	// src/Calculator.g:224:1: forStatement : FOR ^ '(' ! ( forInitClause ) ';' ! ( expression ) ';' ! ( assignmentOrMethodCallStatement ) ')' ! statement ;
	public final CalculatorParser.forStatement_return forStatement() throws RecognitionException {
		CalculatorParser.forStatement_return retval = new CalculatorParser.forStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token FOR43=null;
		Token char_literal44=null;
		Token char_literal46=null;
		Token char_literal48=null;
		Token char_literal50=null;
		ParserRuleReturnScope forInitClause45 =null;
		ParserRuleReturnScope expression47 =null;
		ParserRuleReturnScope assignmentOrMethodCallStatement49 =null;
		ParserRuleReturnScope statement51 =null;

		CommonTree FOR43_tree=null;
		CommonTree char_literal44_tree=null;
		CommonTree char_literal46_tree=null;
		CommonTree char_literal48_tree=null;
		CommonTree char_literal50_tree=null;

		try {
			// src/Calculator.g:225:2: ( FOR ^ '(' ! ( forInitClause ) ';' ! ( expression ) ';' ! ( assignmentOrMethodCallStatement ) ')' ! statement )
			// src/Calculator.g:226:2: FOR ^ '(' ! ( forInitClause ) ';' ! ( expression ) ';' ! ( assignmentOrMethodCallStatement ) ')' ! statement
			{
			root_0 = (CommonTree)adaptor.nil();


			FOR43=(Token)match(input,FOR,FOLLOW_FOR_in_forStatement1183); 
			FOR43_tree = (CommonTree)adaptor.create(FOR43);
			root_0 = (CommonTree)adaptor.becomeRoot(FOR43_tree, root_0);

			char_literal44=(Token)match(input,50,FOLLOW_50_in_forStatement1186); 
			// src/Calculator.g:227:2: ( forInitClause )
			// src/Calculator.g:227:3: forInitClause
			{
			pushFollow(FOLLOW_forInitClause_in_forStatement1191);
			forInitClause45=forInitClause();
			state._fsp--;

			adaptor.addChild(root_0, forInitClause45.getTree());

			}

			char_literal46=(Token)match(input,53,FOLLOW_53_in_forStatement1194); 
			// src/Calculator.g:228:2: ( expression )
			// src/Calculator.g:228:3: expression
			{
			pushFollow(FOLLOW_expression_in_forStatement1199);
			expression47=expression();
			state._fsp--;

			adaptor.addChild(root_0, expression47.getTree());

			}

			char_literal48=(Token)match(input,53,FOLLOW_53_in_forStatement1202); 
			// src/Calculator.g:229:2: ( assignmentOrMethodCallStatement )
			// src/Calculator.g:229:3: assignmentOrMethodCallStatement
			{
			pushFollow(FOLLOW_assignmentOrMethodCallStatement_in_forStatement1207);
			assignmentOrMethodCallStatement49=assignmentOrMethodCallStatement();
			state._fsp--;

			adaptor.addChild(root_0, assignmentOrMethodCallStatement49.getTree());

			}

			char_literal50=(Token)match(input,51,FOLLOW_51_in_forStatement1211); 
			pushFollow(FOLLOW_statement_in_forStatement1215);
			statement51=statement();
			state._fsp--;

			adaptor.addChild(root_0, statement51.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "forStatement"


	public static class forInitClause_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "forInitClause"
	// src/Calculator.g:234:1: forInitClause : ( localVariableDeclaration | variableAssignment );
	public final CalculatorParser.forInitClause_return forInitClause() throws RecognitionException {
		CalculatorParser.forInitClause_return retval = new CalculatorParser.forInitClause_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope localVariableDeclaration52 =null;
		ParserRuleReturnScope variableAssignment53 =null;


		try {
			// src/Calculator.g:235:2: ( localVariableDeclaration | variableAssignment )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( ((LA6_0 >= DT_BOOLEAN && LA6_0 <= DT_STRING)) ) {
				alt6=1;
			}
			else if ( (LA6_0==ID) ) {
				alt6=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// src/Calculator.g:235:4: localVariableDeclaration
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_localVariableDeclaration_in_forInitClause1226);
					localVariableDeclaration52=localVariableDeclaration();
					state._fsp--;

					adaptor.addChild(root_0, localVariableDeclaration52.getTree());

					}
					break;
				case 2 :
					// src/Calculator.g:236:4: variableAssignment
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_variableAssignment_in_forInitClause1231);
					variableAssignment53=variableAssignment();
					state._fsp--;

					adaptor.addChild(root_0, variableAssignment53.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "forInitClause"


	public static class localVariableDeclaration_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "localVariableDeclaration"
	// src/Calculator.g:239:1: localVariableDeclaration : primitiveType variableAssignment ( ',' variableAssignment )* -> ^( VARIABLE_DEF primitiveType variableAssignment ( variableAssignment )* ) ;
	public final CalculatorParser.localVariableDeclaration_return localVariableDeclaration() throws RecognitionException {
		CalculatorParser.localVariableDeclaration_return retval = new CalculatorParser.localVariableDeclaration_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal56=null;
		ParserRuleReturnScope primitiveType54 =null;
		ParserRuleReturnScope variableAssignment55 =null;
		ParserRuleReturnScope variableAssignment57 =null;

		CommonTree char_literal56_tree=null;
		RewriteRuleTokenStream stream_52=new RewriteRuleTokenStream(adaptor,"token 52");
		RewriteRuleSubtreeStream stream_primitiveType=new RewriteRuleSubtreeStream(adaptor,"rule primitiveType");
		RewriteRuleSubtreeStream stream_variableAssignment=new RewriteRuleSubtreeStream(adaptor,"rule variableAssignment");

		try {
			// src/Calculator.g:240:2: ( primitiveType variableAssignment ( ',' variableAssignment )* -> ^( VARIABLE_DEF primitiveType variableAssignment ( variableAssignment )* ) )
			// src/Calculator.g:241:2: primitiveType variableAssignment ( ',' variableAssignment )*
			{
			pushFollow(FOLLOW_primitiveType_in_localVariableDeclaration1243);
			primitiveType54=primitiveType();
			state._fsp--;

			stream_primitiveType.add(primitiveType54.getTree());
			pushFollow(FOLLOW_variableAssignment_in_localVariableDeclaration1246);
			variableAssignment55=variableAssignment();
			state._fsp--;

			stream_variableAssignment.add(variableAssignment55.getTree());
			// src/Calculator.g:243:2: ( ',' variableAssignment )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==52) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// src/Calculator.g:243:3: ',' variableAssignment
					{
					char_literal56=(Token)match(input,52,FOLLOW_52_in_localVariableDeclaration1250);  
					stream_52.add(char_literal56);

					pushFollow(FOLLOW_variableAssignment_in_localVariableDeclaration1252);
					variableAssignment57=variableAssignment();
					state._fsp--;

					stream_variableAssignment.add(variableAssignment57.getTree());
					}
					break;

				default :
					break loop7;
				}
			}

			// AST REWRITE
			// elements: variableAssignment, variableAssignment, primitiveType
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 244:2: -> ^( VARIABLE_DEF primitiveType variableAssignment ( variableAssignment )* )
			{
				// src/Calculator.g:244:5: ^( VARIABLE_DEF primitiveType variableAssignment ( variableAssignment )* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARIABLE_DEF, "VARIABLE_DEF"), root_1);
				adaptor.addChild(root_1, stream_primitiveType.nextTree());
				adaptor.addChild(root_1, stream_variableAssignment.nextTree());
				// src/Calculator.g:244:53: ( variableAssignment )*
				while ( stream_variableAssignment.hasNext() ) {
					adaptor.addChild(root_1, stream_variableAssignment.nextTree());
				}
				stream_variableAssignment.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "localVariableDeclaration"


	public static class variableAssignment_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "variableAssignment"
	// src/Calculator.g:247:1: variableAssignment : ID ( '=' expression )? -> ^( VARIABLE ID ( expression )? ) ;
	public final CalculatorParser.variableAssignment_return variableAssignment() throws RecognitionException {
		CalculatorParser.variableAssignment_return retval = new CalculatorParser.variableAssignment_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID58=null;
		Token char_literal59=null;
		ParserRuleReturnScope expression60 =null;

		CommonTree ID58_tree=null;
		CommonTree char_literal59_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_ASSIGNOP=new RewriteRuleTokenStream(adaptor,"token ASSIGNOP");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// src/Calculator.g:248:2: ( ID ( '=' expression )? -> ^( VARIABLE ID ( expression )? ) )
			// src/Calculator.g:249:2: ID ( '=' expression )?
			{
			ID58=(Token)match(input,ID,FOLLOW_ID_in_variableAssignment1280);  
			stream_ID.add(ID58);

			// src/Calculator.g:250:2: ( '=' expression )?
			int alt8=2;
			int LA8_0 = input.LA(1);
			if ( (LA8_0==ASSIGNOP) ) {
				alt8=1;
			}
			switch (alt8) {
				case 1 :
					// src/Calculator.g:250:3: '=' expression
					{
					char_literal59=(Token)match(input,ASSIGNOP,FOLLOW_ASSIGNOP_in_variableAssignment1284);  
					stream_ASSIGNOP.add(char_literal59);

					pushFollow(FOLLOW_expression_in_variableAssignment1286);
					expression60=expression();
					state._fsp--;

					stream_expression.add(expression60.getTree());
					}
					break;

			}

			// AST REWRITE
			// elements: ID, expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 251:2: -> ^( VARIABLE ID ( expression )? )
			{
				// src/Calculator.g:251:5: ^( VARIABLE ID ( expression )? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(VARIABLE, "VARIABLE"), root_1);
				adaptor.addChild(root_1, stream_ID.nextNode());
				// src/Calculator.g:251:19: ( expression )?
				if ( stream_expression.hasNext() ) {
					adaptor.addChild(root_1, stream_expression.nextTree());
				}
				stream_expression.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "variableAssignment"


	public static class block_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "block"
	// src/Calculator.g:254:1: block : LCURL ( blockStatement )* RCURL -> ^( BLOCK ( blockStatement )* ) ;
	public final CalculatorParser.block_return block() throws RecognitionException {
		CalculatorParser.block_return retval = new CalculatorParser.block_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token LCURL61=null;
		Token RCURL63=null;
		ParserRuleReturnScope blockStatement62 =null;

		CommonTree LCURL61_tree=null;
		CommonTree RCURL63_tree=null;
		RewriteRuleTokenStream stream_LCURL=new RewriteRuleTokenStream(adaptor,"token LCURL");
		RewriteRuleTokenStream stream_RCURL=new RewriteRuleTokenStream(adaptor,"token RCURL");
		RewriteRuleSubtreeStream stream_blockStatement=new RewriteRuleSubtreeStream(adaptor,"rule blockStatement");

		try {
			// src/Calculator.g:254:7: ( LCURL ( blockStatement )* RCURL -> ^( BLOCK ( blockStatement )* ) )
			// src/Calculator.g:255:2: LCURL ( blockStatement )* RCURL
			{
			LCURL61=(Token)match(input,LCURL,FOLLOW_LCURL_in_block1311);  
			stream_LCURL.add(LCURL61);

			// src/Calculator.g:255:8: ( blockStatement )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( ((LA9_0 >= DT_BOOLEAN && LA9_0 <= DT_STRING)||(LA9_0 >= FOR && LA9_0 <= IF)||LA9_0==LCURL||(LA9_0 >= 54 && LA9_0 <= 56)||LA9_0==59) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// src/Calculator.g:255:9: blockStatement
					{
					pushFollow(FOLLOW_blockStatement_in_block1314);
					blockStatement62=blockStatement();
					state._fsp--;

					stream_blockStatement.add(blockStatement62.getTree());
					}
					break;

				default :
					break loop9;
				}
			}

			RCURL63=(Token)match(input,RCURL,FOLLOW_RCURL_in_block1318);  
			stream_RCURL.add(RCURL63);

			// AST REWRITE
			// elements: blockStatement
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 256:2: -> ^( BLOCK ( blockStatement )* )
			{
				// src/Calculator.g:256:5: ^( BLOCK ( blockStatement )* )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(BLOCK, "BLOCK"), root_1);
				// src/Calculator.g:256:13: ( blockStatement )*
				while ( stream_blockStatement.hasNext() ) {
					adaptor.addChild(root_1, stream_blockStatement.nextTree());
				}
				stream_blockStatement.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "block"


	public static class formalParameterList_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameterList"
	// src/Calculator.g:259:1: formalParameterList : '(' ( formalParameters )? ')' -> ^( PARAMS ( formalParameters )? ) ;
	public final CalculatorParser.formalParameterList_return formalParameterList() throws RecognitionException {
		CalculatorParser.formalParameterList_return retval = new CalculatorParser.formalParameterList_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal64=null;
		Token char_literal66=null;
		ParserRuleReturnScope formalParameters65 =null;

		CommonTree char_literal64_tree=null;
		CommonTree char_literal66_tree=null;
		RewriteRuleTokenStream stream_51=new RewriteRuleTokenStream(adaptor,"token 51");
		RewriteRuleTokenStream stream_50=new RewriteRuleTokenStream(adaptor,"token 50");
		RewriteRuleSubtreeStream stream_formalParameters=new RewriteRuleSubtreeStream(adaptor,"rule formalParameters");

		try {
			// src/Calculator.g:260:2: ( '(' ( formalParameters )? ')' -> ^( PARAMS ( formalParameters )? ) )
			// src/Calculator.g:261:2: '(' ( formalParameters )? ')'
			{
			char_literal64=(Token)match(input,50,FOLLOW_50_in_formalParameterList1340);  
			stream_50.add(char_literal64);

			// src/Calculator.g:262:2: ( formalParameters )?
			int alt10=2;
			int LA10_0 = input.LA(1);
			if ( ((LA10_0 >= DT_BOOLEAN && LA10_0 <= DT_STRING)) ) {
				alt10=1;
			}
			switch (alt10) {
				case 1 :
					// src/Calculator.g:262:3: formalParameters
					{
					pushFollow(FOLLOW_formalParameters_in_formalParameterList1344);
					formalParameters65=formalParameters();
					state._fsp--;

					stream_formalParameters.add(formalParameters65.getTree());
					}
					break;

			}

			char_literal66=(Token)match(input,51,FOLLOW_51_in_formalParameterList1349);  
			stream_51.add(char_literal66);

			// AST REWRITE
			// elements: formalParameters
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 264:2: -> ^( PARAMS ( formalParameters )? )
			{
				// src/Calculator.g:264:5: ^( PARAMS ( formalParameters )? )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMS, "PARAMS"), root_1);
				// src/Calculator.g:264:14: ( formalParameters )?
				if ( stream_formalParameters.hasNext() ) {
					adaptor.addChild(root_1, stream_formalParameters.nextTree());
				}
				stream_formalParameters.reset();

				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "formalParameterList"


	public static class formalParameters_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameters"
	// src/Calculator.g:267:1: formalParameters : formalParameter ( ',' ! formalParameter )* ;
	public final CalculatorParser.formalParameters_return formalParameters() throws RecognitionException {
		CalculatorParser.formalParameters_return retval = new CalculatorParser.formalParameters_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal68=null;
		ParserRuleReturnScope formalParameter67 =null;
		ParserRuleReturnScope formalParameter69 =null;

		CommonTree char_literal68_tree=null;

		try {
			// src/Calculator.g:268:2: ( formalParameter ( ',' ! formalParameter )* )
			// src/Calculator.g:269:2: formalParameter ( ',' ! formalParameter )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_formalParameter_in_formalParameters1374);
			formalParameter67=formalParameter();
			state._fsp--;

			adaptor.addChild(root_0, formalParameter67.getTree());

			// src/Calculator.g:270:2: ( ',' ! formalParameter )*
			loop11:
			while (true) {
				int alt11=2;
				int LA11_0 = input.LA(1);
				if ( (LA11_0==52) ) {
					alt11=1;
				}

				switch (alt11) {
				case 1 :
					// src/Calculator.g:270:3: ',' ! formalParameter
					{
					char_literal68=(Token)match(input,52,FOLLOW_52_in_formalParameters1378); 
					pushFollow(FOLLOW_formalParameter_in_formalParameters1381);
					formalParameter69=formalParameter();
					state._fsp--;

					adaptor.addChild(root_0, formalParameter69.getTree());

					}
					break;

				default :
					break loop11;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "formalParameters"


	public static class formalParameter_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "formalParameter"
	// src/Calculator.g:273:1: formalParameter : primitiveType ID -> ^( PARAMETER primitiveType ID ) ;
	public final CalculatorParser.formalParameter_return formalParameter() throws RecognitionException {
		CalculatorParser.formalParameter_return retval = new CalculatorParser.formalParameter_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID71=null;
		ParserRuleReturnScope primitiveType70 =null;

		CommonTree ID71_tree=null;
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleSubtreeStream stream_primitiveType=new RewriteRuleSubtreeStream(adaptor,"rule primitiveType");

		try {
			// src/Calculator.g:274:2: ( primitiveType ID -> ^( PARAMETER primitiveType ID ) )
			// src/Calculator.g:275:2: primitiveType ID
			{
			pushFollow(FOLLOW_primitiveType_in_formalParameter1395);
			primitiveType70=primitiveType();
			state._fsp--;

			stream_primitiveType.add(primitiveType70.getTree());
			ID71=(Token)match(input,ID,FOLLOW_ID_in_formalParameter1398);  
			stream_ID.add(ID71);

			// AST REWRITE
			// elements: primitiveType, ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 276:5: -> ^( PARAMETER primitiveType ID )
			{
				// src/Calculator.g:276:8: ^( PARAMETER primitiveType ID )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(PARAMETER, "PARAMETER"), root_1);
				adaptor.addChild(root_1, stream_primitiveType.nextTree());
				adaptor.addChild(root_1, stream_ID.nextNode());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "formalParameter"


	public static class primitiveType_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "primitiveType"
	// src/Calculator.g:279:1: primitiveType : ( DT_BOOLEAN | DT_INT | DT_STRING | DT_FLOAT );
	public final CalculatorParser.primitiveType_return primitiveType() throws RecognitionException {
		CalculatorParser.primitiveType_return retval = new CalculatorParser.primitiveType_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set72=null;

		CommonTree set72_tree=null;

		try {
			// src/Calculator.g:280:5: ( DT_BOOLEAN | DT_INT | DT_STRING | DT_FLOAT )
			// src/Calculator.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set72=input.LT(1);
			if ( (input.LA(1) >= DT_BOOLEAN && input.LA(1) <= DT_STRING) ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set72));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "primitiveType"


	public static class paranthesisExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "paranthesisExpression"
	// src/Calculator.g:286:1: paranthesisExpression : '(' expression ')' -> ^( EXPRESSION expression ) ;
	public final CalculatorParser.paranthesisExpression_return paranthesisExpression() throws RecognitionException {
		CalculatorParser.paranthesisExpression_return retval = new CalculatorParser.paranthesisExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal73=null;
		Token char_literal75=null;
		ParserRuleReturnScope expression74 =null;

		CommonTree char_literal73_tree=null;
		CommonTree char_literal75_tree=null;
		RewriteRuleTokenStream stream_51=new RewriteRuleTokenStream(adaptor,"token 51");
		RewriteRuleTokenStream stream_50=new RewriteRuleTokenStream(adaptor,"token 50");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");

		try {
			// src/Calculator.g:287:2: ( '(' expression ')' -> ^( EXPRESSION expression ) )
			// src/Calculator.g:288:2: '(' expression ')'
			{
			char_literal73=(Token)match(input,50,FOLLOW_50_in_paranthesisExpression1469);  
			stream_50.add(char_literal73);

			pushFollow(FOLLOW_expression_in_paranthesisExpression1472);
			expression74=expression();
			state._fsp--;

			stream_expression.add(expression74.getTree());
			char_literal75=(Token)match(input,51,FOLLOW_51_in_paranthesisExpression1475);  
			stream_51.add(char_literal75);

			// AST REWRITE
			// elements: expression
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 291:2: -> ^( EXPRESSION expression )
			{
				// src/Calculator.g:291:5: ^( EXPRESSION expression )
				{
				CommonTree root_1 = (CommonTree)adaptor.nil();
				root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(EXPRESSION, "EXPRESSION"), root_1);
				adaptor.addChild(root_1, stream_expression.nextTree());
				adaptor.addChild(root_0, root_1);
				}

			}


			retval.tree = root_0;

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "paranthesisExpression"


	public static class expression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expression"
	// src/Calculator.g:294:1: expression : orExpression ;
	public final CalculatorParser.expression_return expression() throws RecognitionException {
		CalculatorParser.expression_return retval = new CalculatorParser.expression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope orExpression76 =null;


		try {
			// src/Calculator.g:295:2: ( orExpression )
			// src/Calculator.g:296:2: orExpression
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_orExpression_in_expression1496);
			orExpression76=orExpression();
			state._fsp--;

			adaptor.addChild(root_0, orExpression76.getTree());

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "expression"


	public static class assignmentOrMethodCallStatement_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "assignmentOrMethodCallStatement"
	// src/Calculator.g:300:1: assignmentOrMethodCallStatement : ( ID -> ID ) ( '(' ( arguments )? ')' -> ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) ) | assignmentOperator expression -> ^( ASSIGNMENT $assignmentOrMethodCallStatement expression ) ) ;
	public final CalculatorParser.assignmentOrMethodCallStatement_return assignmentOrMethodCallStatement() throws RecognitionException {
		CalculatorParser.assignmentOrMethodCallStatement_return retval = new CalculatorParser.assignmentOrMethodCallStatement_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID77=null;
		Token char_literal78=null;
		Token char_literal80=null;
		ParserRuleReturnScope arguments79 =null;
		ParserRuleReturnScope assignmentOperator81 =null;
		ParserRuleReturnScope expression82 =null;

		CommonTree ID77_tree=null;
		CommonTree char_literal78_tree=null;
		CommonTree char_literal80_tree=null;
		RewriteRuleTokenStream stream_51=new RewriteRuleTokenStream(adaptor,"token 51");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_50=new RewriteRuleTokenStream(adaptor,"token 50");
		RewriteRuleSubtreeStream stream_assignmentOperator=new RewriteRuleSubtreeStream(adaptor,"rule assignmentOperator");
		RewriteRuleSubtreeStream stream_expression=new RewriteRuleSubtreeStream(adaptor,"rule expression");
		RewriteRuleSubtreeStream stream_arguments=new RewriteRuleSubtreeStream(adaptor,"rule arguments");

		try {
			// src/Calculator.g:301:2: ( ( ID -> ID ) ( '(' ( arguments )? ')' -> ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) ) | assignmentOperator expression -> ^( ASSIGNMENT $assignmentOrMethodCallStatement expression ) ) )
			// src/Calculator.g:302:2: ( ID -> ID ) ( '(' ( arguments )? ')' -> ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) ) | assignmentOperator expression -> ^( ASSIGNMENT $assignmentOrMethodCallStatement expression ) )
			{
			// src/Calculator.g:302:2: ( ID -> ID )
			// src/Calculator.g:302:3: ID
			{
			ID77=(Token)match(input,ID,FOLLOW_ID_in_assignmentOrMethodCallStatement1509);  
			stream_ID.add(ID77);

			// AST REWRITE
			// elements: ID
			// token labels: 
			// rule labels: retval
			// token list labels: 
			// rule list labels: 
			// wildcard labels: 
			retval.tree = root_0;
			RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

			root_0 = (CommonTree)adaptor.nil();
			// 302:5: -> ID
			{
				adaptor.addChild(root_0, stream_ID.nextNode());
			}


			retval.tree = root_0;

			}

			// src/Calculator.g:303:2: ( '(' ( arguments )? ')' -> ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) ) | assignmentOperator expression -> ^( ASSIGNMENT $assignmentOrMethodCallStatement expression ) )
			int alt13=2;
			int LA13_0 = input.LA(1);
			if ( (LA13_0==50) ) {
				alt13=1;
			}
			else if ( (LA13_0==ASSIGNOP) ) {
				alt13=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}

			switch (alt13) {
				case 1 :
					// src/Calculator.g:303:4: '(' ( arguments )? ')'
					{
					char_literal78=(Token)match(input,50,FOLLOW_50_in_assignmentOrMethodCallStatement1518);  
					stream_50.add(char_literal78);

					// src/Calculator.g:303:8: ( arguments )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0==BOOL||LA12_0==FLOAT||LA12_0==ID||LA12_0==INT||LA12_0==NOT||LA12_0==PLUSMINUS||LA12_0==STRING||LA12_0==50) ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// src/Calculator.g:303:9: arguments
							{
							pushFollow(FOLLOW_arguments_in_assignmentOrMethodCallStatement1521);
							arguments79=arguments();
							state._fsp--;

							stream_arguments.add(arguments79.getTree());
							}
							break;

					}

					char_literal80=(Token)match(input,51,FOLLOW_51_in_assignmentOrMethodCallStatement1525);  
					stream_51.add(char_literal80);

					// AST REWRITE
					// elements: assignmentOrMethodCallStatement, arguments
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 303:26: -> ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) )
					{
						// src/Calculator.g:303:29: ^( METHOD_CALL $assignmentOrMethodCallStatement ^( ARGS ( arguments )? ) )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(METHOD_CALL, "METHOD_CALL"), root_1);
						adaptor.addChild(root_1, stream_retval.nextTree());
						// src/Calculator.g:303:76: ^( ARGS ( arguments )? )
						{
						CommonTree root_2 = (CommonTree)adaptor.nil();
						root_2 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ARGS, "ARGS"), root_2);
						// src/Calculator.g:303:83: ( arguments )?
						if ( stream_arguments.hasNext() ) {
							adaptor.addChild(root_2, stream_arguments.nextTree());
						}
						stream_arguments.reset();

						adaptor.addChild(root_1, root_2);
						}

						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// src/Calculator.g:304:4: assignmentOperator expression
					{
					pushFollow(FOLLOW_assignmentOperator_in_assignmentOrMethodCallStatement1551);
					assignmentOperator81=assignmentOperator();
					state._fsp--;

					stream_assignmentOperator.add(assignmentOperator81.getTree());
					pushFollow(FOLLOW_expression_in_assignmentOrMethodCallStatement1553);
					expression82=expression();
					state._fsp--;

					stream_expression.add(expression82.getTree());
					// AST REWRITE
					// elements: assignmentOrMethodCallStatement, expression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 304:34: -> ^( ASSIGNMENT $assignmentOrMethodCallStatement expression )
					{
						// src/Calculator.g:304:37: ^( ASSIGNMENT $assignmentOrMethodCallStatement expression )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(ASSIGNMENT, "ASSIGNMENT"), root_1);
						adaptor.addChild(root_1, stream_retval.nextTree());
						adaptor.addChild(root_1, stream_expression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignmentOrMethodCallStatement"


	public static class assignmentOperator_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "assignmentOperator"
	// src/Calculator.g:308:1: assignmentOperator : '=' ;
	public final CalculatorParser.assignmentOperator_return assignmentOperator() throws RecognitionException {
		CalculatorParser.assignmentOperator_return retval = new CalculatorParser.assignmentOperator_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal83=null;

		CommonTree char_literal83_tree=null;

		try {
			// src/Calculator.g:309:2: ( '=' )
			// src/Calculator.g:310:2: '='
			{
			root_0 = (CommonTree)adaptor.nil();


			char_literal83=(Token)match(input,ASSIGNOP,FOLLOW_ASSIGNOP_in_assignmentOperator1582); 
			char_literal83_tree = (CommonTree)adaptor.create(char_literal83);
			adaptor.addChild(root_0, char_literal83_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "assignmentOperator"


	public static class orExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "orExpression"
	// src/Calculator.g:313:1: orExpression : andExpression ( '||' ^ andExpression )* ;
	public final CalculatorParser.orExpression_return orExpression() throws RecognitionException {
		CalculatorParser.orExpression_return retval = new CalculatorParser.orExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal85=null;
		ParserRuleReturnScope andExpression84 =null;
		ParserRuleReturnScope andExpression86 =null;

		CommonTree string_literal85_tree=null;

		try {
			// src/Calculator.g:314:2: ( andExpression ( '||' ^ andExpression )* )
			// src/Calculator.g:315:2: andExpression ( '||' ^ andExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_andExpression_in_orExpression1594);
			andExpression84=andExpression();
			state._fsp--;

			adaptor.addChild(root_0, andExpression84.getTree());

			// src/Calculator.g:316:2: ( '||' ^ andExpression )*
			loop14:
			while (true) {
				int alt14=2;
				int LA14_0 = input.LA(1);
				if ( (LA14_0==OROP) ) {
					alt14=1;
				}

				switch (alt14) {
				case 1 :
					// src/Calculator.g:316:3: '||' ^ andExpression
					{
					string_literal85=(Token)match(input,OROP,FOLLOW_OROP_in_orExpression1598); 
					string_literal85_tree = (CommonTree)adaptor.create(string_literal85);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal85_tree, root_0);

					pushFollow(FOLLOW_andExpression_in_orExpression1601);
					andExpression86=andExpression();
					state._fsp--;

					adaptor.addChild(root_0, andExpression86.getTree());

					}
					break;

				default :
					break loop14;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "orExpression"


	public static class andExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "andExpression"
	// src/Calculator.g:320:1: andExpression : relExpression ( '&&' ^ relExpression )* ;
	public final CalculatorParser.andExpression_return andExpression() throws RecognitionException {
		CalculatorParser.andExpression_return retval = new CalculatorParser.andExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token string_literal88=null;
		ParserRuleReturnScope relExpression87 =null;
		ParserRuleReturnScope relExpression89 =null;

		CommonTree string_literal88_tree=null;

		try {
			// src/Calculator.g:321:2: ( relExpression ( '&&' ^ relExpression )* )
			// src/Calculator.g:321:4: relExpression ( '&&' ^ relExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_relExpression_in_andExpression1617);
			relExpression87=relExpression();
			state._fsp--;

			adaptor.addChild(root_0, relExpression87.getTree());

			// src/Calculator.g:322:2: ( '&&' ^ relExpression )*
			loop15:
			while (true) {
				int alt15=2;
				int LA15_0 = input.LA(1);
				if ( (LA15_0==ANDOP) ) {
					alt15=1;
				}

				switch (alt15) {
				case 1 :
					// src/Calculator.g:322:3: '&&' ^ relExpression
					{
					string_literal88=(Token)match(input,ANDOP,FOLLOW_ANDOP_in_andExpression1621); 
					string_literal88_tree = (CommonTree)adaptor.create(string_literal88);
					root_0 = (CommonTree)adaptor.becomeRoot(string_literal88_tree, root_0);

					pushFollow(FOLLOW_relExpression_in_andExpression1624);
					relExpression89=relExpression();
					state._fsp--;

					adaptor.addChild(root_0, relExpression89.getTree());

					}
					break;

				default :
					break loop15;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "andExpression"


	public static class relExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "relExpression"
	// src/Calculator.g:325:1: relExpression : addExpression ( RELOP ^ addExpression )* ;
	public final CalculatorParser.relExpression_return relExpression() throws RecognitionException {
		CalculatorParser.relExpression_return retval = new CalculatorParser.relExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token RELOP91=null;
		ParserRuleReturnScope addExpression90 =null;
		ParserRuleReturnScope addExpression92 =null;

		CommonTree RELOP91_tree=null;

		try {
			// src/Calculator.g:326:2: ( addExpression ( RELOP ^ addExpression )* )
			// src/Calculator.g:326:4: addExpression ( RELOP ^ addExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_addExpression_in_relExpression1637);
			addExpression90=addExpression();
			state._fsp--;

			adaptor.addChild(root_0, addExpression90.getTree());

			// src/Calculator.g:327:2: ( RELOP ^ addExpression )*
			loop16:
			while (true) {
				int alt16=2;
				int LA16_0 = input.LA(1);
				if ( (LA16_0==RELOP) ) {
					alt16=1;
				}

				switch (alt16) {
				case 1 :
					// src/Calculator.g:327:3: RELOP ^ addExpression
					{
					RELOP91=(Token)match(input,RELOP,FOLLOW_RELOP_in_relExpression1641); 
					RELOP91_tree = (CommonTree)adaptor.create(RELOP91);
					root_0 = (CommonTree)adaptor.becomeRoot(RELOP91_tree, root_0);

					pushFollow(FOLLOW_addExpression_in_relExpression1644);
					addExpression92=addExpression();
					state._fsp--;

					adaptor.addChild(root_0, addExpression92.getTree());

					}
					break;

				default :
					break loop16;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "relExpression"


	public static class addExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "addExpression"
	// src/Calculator.g:330:1: addExpression : multiplyExpression ( PLUSMINUS ^ multiplyExpression )* ;
	public final CalculatorParser.addExpression_return addExpression() throws RecognitionException {
		CalculatorParser.addExpression_return retval = new CalculatorParser.addExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUSMINUS94=null;
		ParserRuleReturnScope multiplyExpression93 =null;
		ParserRuleReturnScope multiplyExpression95 =null;

		CommonTree PLUSMINUS94_tree=null;

		try {
			// src/Calculator.g:331:2: ( multiplyExpression ( PLUSMINUS ^ multiplyExpression )* )
			// src/Calculator.g:332:2: multiplyExpression ( PLUSMINUS ^ multiplyExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_multiplyExpression_in_addExpression1658);
			multiplyExpression93=multiplyExpression();
			state._fsp--;

			adaptor.addChild(root_0, multiplyExpression93.getTree());

			// src/Calculator.g:333:2: ( PLUSMINUS ^ multiplyExpression )*
			loop17:
			while (true) {
				int alt17=2;
				int LA17_0 = input.LA(1);
				if ( (LA17_0==PLUSMINUS) ) {
					alt17=1;
				}

				switch (alt17) {
				case 1 :
					// src/Calculator.g:333:3: PLUSMINUS ^ multiplyExpression
					{
					PLUSMINUS94=(Token)match(input,PLUSMINUS,FOLLOW_PLUSMINUS_in_addExpression1662); 
					PLUSMINUS94_tree = (CommonTree)adaptor.create(PLUSMINUS94);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUSMINUS94_tree, root_0);

					pushFollow(FOLLOW_multiplyExpression_in_addExpression1665);
					multiplyExpression95=multiplyExpression();
					state._fsp--;

					adaptor.addChild(root_0, multiplyExpression95.getTree());

					}
					break;

				default :
					break loop17;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "addExpression"


	public static class multiplyExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multiplyExpression"
	// src/Calculator.g:336:1: multiplyExpression : unaryExpression ( MULTIPLYOP ^ unaryExpression )* ;
	public final CalculatorParser.multiplyExpression_return multiplyExpression() throws RecognitionException {
		CalculatorParser.multiplyExpression_return retval = new CalculatorParser.multiplyExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MULTIPLYOP97=null;
		ParserRuleReturnScope unaryExpression96 =null;
		ParserRuleReturnScope unaryExpression98 =null;

		CommonTree MULTIPLYOP97_tree=null;

		try {
			// src/Calculator.g:337:2: ( unaryExpression ( MULTIPLYOP ^ unaryExpression )* )
			// src/Calculator.g:338:2: unaryExpression ( MULTIPLYOP ^ unaryExpression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_unaryExpression_in_multiplyExpression1679);
			unaryExpression96=unaryExpression();
			state._fsp--;

			adaptor.addChild(root_0, unaryExpression96.getTree());

			// src/Calculator.g:339:2: ( MULTIPLYOP ^ unaryExpression )*
			loop18:
			while (true) {
				int alt18=2;
				int LA18_0 = input.LA(1);
				if ( (LA18_0==MULTIPLYOP) ) {
					alt18=1;
				}

				switch (alt18) {
				case 1 :
					// src/Calculator.g:339:3: MULTIPLYOP ^ unaryExpression
					{
					MULTIPLYOP97=(Token)match(input,MULTIPLYOP,FOLLOW_MULTIPLYOP_in_multiplyExpression1683); 
					MULTIPLYOP97_tree = (CommonTree)adaptor.create(MULTIPLYOP97);
					root_0 = (CommonTree)adaptor.becomeRoot(MULTIPLYOP97_tree, root_0);

					pushFollow(FOLLOW_unaryExpression_in_multiplyExpression1686);
					unaryExpression98=unaryExpression();
					state._fsp--;

					adaptor.addChild(root_0, unaryExpression98.getTree());

					}
					break;

				default :
					break loop18;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "multiplyExpression"


	public static class unaryExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "unaryExpression"
	// src/Calculator.g:342:1: unaryExpression : ( PLUSMINUS notExpression -> ^( PLUSMINUS notExpression ) | notExpression );
	public final CalculatorParser.unaryExpression_return unaryExpression() throws RecognitionException {
		CalculatorParser.unaryExpression_return retval = new CalculatorParser.unaryExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUSMINUS99=null;
		ParserRuleReturnScope notExpression100 =null;
		ParserRuleReturnScope notExpression101 =null;

		CommonTree PLUSMINUS99_tree=null;
		RewriteRuleTokenStream stream_PLUSMINUS=new RewriteRuleTokenStream(adaptor,"token PLUSMINUS");
		RewriteRuleSubtreeStream stream_notExpression=new RewriteRuleSubtreeStream(adaptor,"rule notExpression");

		try {
			// src/Calculator.g:343:2: ( PLUSMINUS notExpression -> ^( PLUSMINUS notExpression ) | notExpression )
			int alt19=2;
			int LA19_0 = input.LA(1);
			if ( (LA19_0==PLUSMINUS) ) {
				alt19=1;
			}
			else if ( (LA19_0==BOOL||LA19_0==FLOAT||LA19_0==ID||LA19_0==INT||LA19_0==NOT||LA19_0==STRING||LA19_0==50) ) {
				alt19=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 19, 0, input);
				throw nvae;
			}

			switch (alt19) {
				case 1 :
					// src/Calculator.g:343:4: PLUSMINUS notExpression
					{
					PLUSMINUS99=(Token)match(input,PLUSMINUS,FOLLOW_PLUSMINUS_in_unaryExpression1699);  
					stream_PLUSMINUS.add(PLUSMINUS99);

					pushFollow(FOLLOW_notExpression_in_unaryExpression1701);
					notExpression100=notExpression();
					state._fsp--;

					stream_notExpression.add(notExpression100.getTree());
					// AST REWRITE
					// elements: notExpression, PLUSMINUS
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 343:28: -> ^( PLUSMINUS notExpression )
					{
						// src/Calculator.g:343:31: ^( PLUSMINUS notExpression )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PLUSMINUS.nextNode(), root_1);
						adaptor.addChild(root_1, stream_notExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// src/Calculator.g:344:8: notExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_notExpression_in_unaryExpression1718);
					notExpression101=notExpression();
					state._fsp--;

					adaptor.addChild(root_0, notExpression101.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "unaryExpression"


	public static class notExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "notExpression"
	// src/Calculator.g:347:1: notExpression : ( '!' basicExpression -> ^( NOT basicExpression ) | basicExpression );
	public final CalculatorParser.notExpression_return notExpression() throws RecognitionException {
		CalculatorParser.notExpression_return retval = new CalculatorParser.notExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal102=null;
		ParserRuleReturnScope basicExpression103 =null;
		ParserRuleReturnScope basicExpression104 =null;

		CommonTree char_literal102_tree=null;
		RewriteRuleTokenStream stream_NOT=new RewriteRuleTokenStream(adaptor,"token NOT");
		RewriteRuleSubtreeStream stream_basicExpression=new RewriteRuleSubtreeStream(adaptor,"rule basicExpression");

		try {
			// src/Calculator.g:348:2: ( '!' basicExpression -> ^( NOT basicExpression ) | basicExpression )
			int alt20=2;
			int LA20_0 = input.LA(1);
			if ( (LA20_0==NOT) ) {
				alt20=1;
			}
			else if ( (LA20_0==BOOL||LA20_0==FLOAT||LA20_0==ID||LA20_0==INT||LA20_0==STRING||LA20_0==50) ) {
				alt20=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 20, 0, input);
				throw nvae;
			}

			switch (alt20) {
				case 1 :
					// src/Calculator.g:348:4: '!' basicExpression
					{
					char_literal102=(Token)match(input,NOT,FOLLOW_NOT_in_notExpression1729);  
					stream_NOT.add(char_literal102);

					pushFollow(FOLLOW_basicExpression_in_notExpression1731);
					basicExpression103=basicExpression();
					state._fsp--;

					stream_basicExpression.add(basicExpression103.getTree());
					// AST REWRITE
					// elements: basicExpression
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 348:24: -> ^( NOT basicExpression )
					{
						// src/Calculator.g:348:27: ^( NOT basicExpression )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(NOT, "NOT"), root_1);
						adaptor.addChild(root_1, stream_basicExpression.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					// src/Calculator.g:349:4: basicExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_basicExpression_in_notExpression1744);
					basicExpression104=basicExpression();
					state._fsp--;

					adaptor.addChild(root_0, basicExpression104.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "notExpression"


	public static class basicExpression_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "basicExpression"
	// src/Calculator.g:352:1: basicExpression : ( paranthesisExpression | variableAccessOrMethodCall | literal );
	public final CalculatorParser.basicExpression_return basicExpression() throws RecognitionException {
		CalculatorParser.basicExpression_return retval = new CalculatorParser.basicExpression_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		ParserRuleReturnScope paranthesisExpression105 =null;
		ParserRuleReturnScope variableAccessOrMethodCall106 =null;
		ParserRuleReturnScope literal107 =null;


		try {
			// src/Calculator.g:353:2: ( paranthesisExpression | variableAccessOrMethodCall | literal )
			int alt21=3;
			switch ( input.LA(1) ) {
			case 50:
				{
				alt21=1;
				}
				break;
			case ID:
				{
				alt21=2;
				}
				break;
			case BOOL:
			case FLOAT:
			case INT:
			case STRING:
				{
				alt21=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 21, 0, input);
				throw nvae;
			}
			switch (alt21) {
				case 1 :
					// src/Calculator.g:353:4: paranthesisExpression
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_paranthesisExpression_in_basicExpression1755);
					paranthesisExpression105=paranthesisExpression();
					state._fsp--;

					adaptor.addChild(root_0, paranthesisExpression105.getTree());

					}
					break;
				case 2 :
					// src/Calculator.g:354:4: variableAccessOrMethodCall
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_variableAccessOrMethodCall_in_basicExpression1760);
					variableAccessOrMethodCall106=variableAccessOrMethodCall();
					state._fsp--;

					adaptor.addChild(root_0, variableAccessOrMethodCall106.getTree());

					}
					break;
				case 3 :
					// src/Calculator.g:355:4: literal
					{
					root_0 = (CommonTree)adaptor.nil();


					pushFollow(FOLLOW_literal_in_basicExpression1765);
					literal107=literal();
					state._fsp--;

					adaptor.addChild(root_0, literal107.getTree());

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "basicExpression"


	public static class variableAccessOrMethodCall_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "variableAccessOrMethodCall"
	// src/Calculator.g:358:1: variableAccessOrMethodCall : ID ( '(' ( arguments )? ')' )? ;
	public final CalculatorParser.variableAccessOrMethodCall_return variableAccessOrMethodCall() throws RecognitionException {
		CalculatorParser.variableAccessOrMethodCall_return retval = new CalculatorParser.variableAccessOrMethodCall_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token ID108=null;
		Token char_literal109=null;
		Token char_literal111=null;
		ParserRuleReturnScope arguments110 =null;

		CommonTree ID108_tree=null;
		CommonTree char_literal109_tree=null;
		CommonTree char_literal111_tree=null;

		try {
			// src/Calculator.g:359:2: ( ID ( '(' ( arguments )? ')' )? )
			// src/Calculator.g:360:2: ID ( '(' ( arguments )? ')' )?
			{
			root_0 = (CommonTree)adaptor.nil();


			ID108=(Token)match(input,ID,FOLLOW_ID_in_variableAccessOrMethodCall1777); 
			ID108_tree = (CommonTree)adaptor.create(ID108);
			adaptor.addChild(root_0, ID108_tree);

			// src/Calculator.g:361:2: ( '(' ( arguments )? ')' )?
			int alt23=2;
			int LA23_0 = input.LA(1);
			if ( (LA23_0==50) ) {
				alt23=1;
			}
			switch (alt23) {
				case 1 :
					// src/Calculator.g:361:3: '(' ( arguments )? ')'
					{
					char_literal109=(Token)match(input,50,FOLLOW_50_in_variableAccessOrMethodCall1781); 
					char_literal109_tree = (CommonTree)adaptor.create(char_literal109);
					adaptor.addChild(root_0, char_literal109_tree);

					// src/Calculator.g:361:7: ( arguments )?
					int alt22=2;
					int LA22_0 = input.LA(1);
					if ( (LA22_0==BOOL||LA22_0==FLOAT||LA22_0==ID||LA22_0==INT||LA22_0==NOT||LA22_0==PLUSMINUS||LA22_0==STRING||LA22_0==50) ) {
						alt22=1;
					}
					switch (alt22) {
						case 1 :
							// src/Calculator.g:361:8: arguments
							{
							pushFollow(FOLLOW_arguments_in_variableAccessOrMethodCall1784);
							arguments110=arguments();
							state._fsp--;

							adaptor.addChild(root_0, arguments110.getTree());

							}
							break;

					}

					char_literal111=(Token)match(input,51,FOLLOW_51_in_variableAccessOrMethodCall1788); 
					char_literal111_tree = (CommonTree)adaptor.create(char_literal111);
					adaptor.addChild(root_0, char_literal111_tree);

					}
					break;

			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "variableAccessOrMethodCall"


	public static class arguments_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "arguments"
	// src/Calculator.g:364:1: arguments : expression ( ',' ! expression )* ;
	public final CalculatorParser.arguments_return arguments() throws RecognitionException {
		CalculatorParser.arguments_return retval = new CalculatorParser.arguments_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token char_literal113=null;
		ParserRuleReturnScope expression112 =null;
		ParserRuleReturnScope expression114 =null;

		CommonTree char_literal113_tree=null;

		try {
			// src/Calculator.g:365:2: ( expression ( ',' ! expression )* )
			// src/Calculator.g:366:2: expression ( ',' ! expression )*
			{
			root_0 = (CommonTree)adaptor.nil();


			pushFollow(FOLLOW_expression_in_arguments1802);
			expression112=expression();
			state._fsp--;

			adaptor.addChild(root_0, expression112.getTree());

			// src/Calculator.g:367:2: ( ',' ! expression )*
			loop24:
			while (true) {
				int alt24=2;
				int LA24_0 = input.LA(1);
				if ( (LA24_0==52) ) {
					alt24=1;
				}

				switch (alt24) {
				case 1 :
					// src/Calculator.g:367:3: ',' ! expression
					{
					char_literal113=(Token)match(input,52,FOLLOW_52_in_arguments1806); 
					pushFollow(FOLLOW_expression_in_arguments1809);
					expression114=expression();
					state._fsp--;

					adaptor.addChild(root_0, expression114.getTree());

					}
					break;

				default :
					break loop24;
				}
			}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "arguments"


	public static class literal_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "literal"
	// src/Calculator.g:370:1: literal : ( BOOL | STRING | INT | FLOAT );
	public final CalculatorParser.literal_return literal() throws RecognitionException {
		CalculatorParser.literal_return retval = new CalculatorParser.literal_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token set115=null;

		CommonTree set115_tree=null;

		try {
			// src/Calculator.g:371:2: ( BOOL | STRING | INT | FLOAT )
			// src/Calculator.g:
			{
			root_0 = (CommonTree)adaptor.nil();


			set115=input.LT(1);
			if ( input.LA(1)==BOOL||input.LA(1)==FLOAT||input.LA(1)==INT||input.LA(1)==STRING ) {
				input.consume();
				adaptor.addChild(root_0, (CommonTree)adaptor.create(set115));
				state.errorRecovery=false;
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				throw mse;
			}
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		return retval;
	}
	// $ANTLR end "literal"

	// Delegated rules



	public static final BitSet FOLLOW_methodDeclarations_in_program756 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_methodDeclaration_in_methodDeclarations776 = new BitSet(new long[]{0x00000000000F0002L});
	public static final BitSet FOLLOW_methodHeader_in_methodDeclaration792 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_methodBody_in_methodDeclaration798 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_methodHeader826 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_methodHeader832 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_formalParameterList_in_methodHeader838 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LCURL_in_methodBody860 = new BitSet(new long[]{0x0DC000000B8F0000L});
	public static final BitSet FOLLOW_blockStatement_in_methodBody868 = new BitSet(new long[]{0x0DC000000B8F0000L});
	public static final BitSet FOLLOW_returnStatement_in_methodBody877 = new BitSet(new long[]{0x0000020000000000L});
	public static final BitSet FOLLOW_RCURL_in_methodBody884 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_58_in_returnStatement925 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_returnStatement927 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_returnStatement929 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclarationStatement_in_blockStatement953 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_statement_in_blockStatement963 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ifStatement_in_statement977 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_forStatement_in_statement982 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_block_in_statement987 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_whileStatement_in_statement992 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_56_in_statement1001 = new BitSet(new long[]{0x09C000000B800000L});
	public static final BitSet FOLLOW_statement_in_statement1004 = new BitSet(new long[]{0x0800000000000000L});
	public static final BitSet FOLLOW_59_in_statement1006 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_paranthesisExpression_in_statement1009 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_statement1011 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_54_in_statement1021 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_statement1024 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_55_in_statement1034 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_statement1037 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentOrMethodCallStatement_in_statement1048 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_statement1050 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_59_in_whileStatement1064 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_paranthesisExpression_in_whileStatement1066 = new BitSet(new long[]{0x09C000000B800000L});
	public static final BitSet FOLLOW_statement_in_whileStatement1068 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclaration_in_localVariableDeclarationStatement1096 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_localVariableDeclarationStatement1107 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_ifStatement1122 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_paranthesisExpression_in_ifStatement1124 = new BitSet(new long[]{0x09C000000B800000L});
	public static final BitSet FOLLOW_statement_in_ifStatement1126 = new BitSet(new long[]{0x0200000000000002L});
	public static final BitSet FOLLOW_elseStatement_in_ifStatement1140 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_57_in_elseStatement1167 = new BitSet(new long[]{0x09C000000B800000L});
	public static final BitSet FOLLOW_statement_in_elseStatement1170 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FOR_in_forStatement1183 = new BitSet(new long[]{0x0004000000000000L});
	public static final BitSet FOLLOW_50_in_forStatement1186 = new BitSet(new long[]{0x00000000010F0000L});
	public static final BitSet FOLLOW_forInitClause_in_forStatement1191 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_forStatement1194 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_forStatement1199 = new BitSet(new long[]{0x0020000000000000L});
	public static final BitSet FOLLOW_53_in_forStatement1202 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_assignmentOrMethodCallStatement_in_forStatement1207 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_forStatement1211 = new BitSet(new long[]{0x09C000000B800000L});
	public static final BitSet FOLLOW_statement_in_forStatement1215 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_localVariableDeclaration_in_forInitClause1226 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableAssignment_in_forInitClause1231 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_localVariableDeclaration1243 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_variableAssignment_in_localVariableDeclaration1246 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_52_in_localVariableDeclaration1250 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_variableAssignment_in_localVariableDeclaration1252 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_ID_in_variableAssignment1280 = new BitSet(new long[]{0x0000000000000202L});
	public static final BitSet FOLLOW_ASSIGNOP_in_variableAssignment1284 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_variableAssignment1286 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LCURL_in_block1311 = new BitSet(new long[]{0x09C002000B8F0000L});
	public static final BitSet FOLLOW_blockStatement_in_block1314 = new BitSet(new long[]{0x09C002000B8F0000L});
	public static final BitSet FOLLOW_RCURL_in_block1318 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_50_in_formalParameterList1340 = new BitSet(new long[]{0x00080000000F0000L});
	public static final BitSet FOLLOW_formalParameters_in_formalParameterList1344 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_formalParameterList1349 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_formalParameter_in_formalParameters1374 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_52_in_formalParameters1378 = new BitSet(new long[]{0x00000000000F0000L});
	public static final BitSet FOLLOW_formalParameter_in_formalParameters1381 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_primitiveType_in_formalParameter1395 = new BitSet(new long[]{0x0000000001000000L});
	public static final BitSet FOLLOW_ID_in_formalParameter1398 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_50_in_paranthesisExpression1469 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_paranthesisExpression1472 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_paranthesisExpression1475 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_orExpression_in_expression1496 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_assignmentOrMethodCallStatement1509 = new BitSet(new long[]{0x0004000000000200L});
	public static final BitSet FOLLOW_50_in_assignmentOrMethodCallStatement1518 = new BitSet(new long[]{0x000C208405400800L});
	public static final BitSet FOLLOW_arguments_in_assignmentOrMethodCallStatement1521 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_assignmentOrMethodCallStatement1525 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_assignmentOperator_in_assignmentOrMethodCallStatement1551 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_assignmentOrMethodCallStatement1553 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ASSIGNOP_in_assignmentOperator1582 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_andExpression_in_orExpression1594 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_OROP_in_orExpression1598 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_andExpression_in_orExpression1601 = new BitSet(new long[]{0x0000001000000002L});
	public static final BitSet FOLLOW_relExpression_in_andExpression1617 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_ANDOP_in_andExpression1621 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_relExpression_in_andExpression1624 = new BitSet(new long[]{0x0000000000000022L});
	public static final BitSet FOLLOW_addExpression_in_relExpression1637 = new BitSet(new long[]{0x0000040000000002L});
	public static final BitSet FOLLOW_RELOP_in_relExpression1641 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_addExpression_in_relExpression1644 = new BitSet(new long[]{0x0000040000000002L});
	public static final BitSet FOLLOW_multiplyExpression_in_addExpression1658 = new BitSet(new long[]{0x0000008000000002L});
	public static final BitSet FOLLOW_PLUSMINUS_in_addExpression1662 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_multiplyExpression_in_addExpression1665 = new BitSet(new long[]{0x0000008000000002L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplyExpression1679 = new BitSet(new long[]{0x0000000100000002L});
	public static final BitSet FOLLOW_MULTIPLYOP_in_multiplyExpression1683 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_unaryExpression_in_multiplyExpression1686 = new BitSet(new long[]{0x0000000100000002L});
	public static final BitSet FOLLOW_PLUSMINUS_in_unaryExpression1699 = new BitSet(new long[]{0x0004200405400800L});
	public static final BitSet FOLLOW_notExpression_in_unaryExpression1701 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_notExpression_in_unaryExpression1718 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NOT_in_notExpression1729 = new BitSet(new long[]{0x0004200005400800L});
	public static final BitSet FOLLOW_basicExpression_in_notExpression1731 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_basicExpression_in_notExpression1744 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_paranthesisExpression_in_basicExpression1755 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_variableAccessOrMethodCall_in_basicExpression1760 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_literal_in_basicExpression1765 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_variableAccessOrMethodCall1777 = new BitSet(new long[]{0x0004000000000002L});
	public static final BitSet FOLLOW_50_in_variableAccessOrMethodCall1781 = new BitSet(new long[]{0x000C208405400800L});
	public static final BitSet FOLLOW_arguments_in_variableAccessOrMethodCall1784 = new BitSet(new long[]{0x0008000000000000L});
	public static final BitSet FOLLOW_51_in_variableAccessOrMethodCall1788 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expression_in_arguments1802 = new BitSet(new long[]{0x0010000000000002L});
	public static final BitSet FOLLOW_52_in_arguments1806 = new BitSet(new long[]{0x0004208405400800L});
	public static final BitSet FOLLOW_expression_in_arguments1809 = new BitSet(new long[]{0x0010000000000002L});
}
