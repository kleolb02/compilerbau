package at.tugraz.ist.cc;
import java.io.File;
import at.tugraz.ist.cc.interfaces.LexicalAndSyntaxAnalyzerInterface;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.ParseTree;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class LexicalAndSyntaxAnalyzer implements LexicalAndSyntaxAnalyzerInterface {

    private org.antlr.runtime.tree.CommonTree tree;

    public int lexer(String file_path, boolean debug) {
    	try {
            CalculatorLexer lex = getLexer(file_path);
            CommonTokenStream tokens = new CommonTokenStream(lex);
            lex.consumeUntil(tokens, CalculatorLexer.EOF);

            List<RecognitionException> errors = lex.getAllErrors();
            
			String[] uri = lex.getSourceName().split("\\.");
			File outputFile = new File(uri[0]+ ".out");
			FileWriter out = new FileWriter(outputFile);
			
            if (debug)
                out.write(  LoggingHelpers.logTokens(tokens));
            
            String outmsg = LoggingHelpers.printErrors(lex, errors, "lexical");
            out.write(outmsg);

            out.flush();
            out.close();
            if (errors.size() > 0)
                return errors.size();

        } catch (IOException e) {
            e.printStackTrace();
            return Integer.MAX_VALUE;
        }

    	return 0;
    }

    public int checkSyntax(String file_path, boolean debug) {
        try {
            CalculatorLexer lex = getLexer(file_path);
            CalculatorParser parser = new CalculatorParser(new CommonTokenStream(lex));
            CalculatorParser.program_return ret = parser.program();		// masc  'program' ist definiert in Calculator.g, da beginnt das

            List<RecognitionException> errors = lex.getAllErrors();
            LoggingHelpers.printErrors(lex, errors, "lexical");

            if (errors.size() > 0) {
                return errors.size();
            }
            errors = parser.getAllErrors();
            String parseErrors = LoggingHelpers.printErrors(lex, errors, "parser");
            tree = ret.getTree();

            if (errors.size() > 0) {
                LoggingHelpers.writeOutFile(lex, parseErrors);
                return errors.size();
            }

            if(debug) {
                String funclDecl = LoggingHelpers.getAllMethodDeclarations(tree);
                String outMsg = funclDecl;
                outMsg += "\r\n";
                outMsg += LoggingHelpers.getTreeStr(tree);
                System.out.println(outMsg);
                LoggingHelpers.writeOutFile(lex, outMsg);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return Integer.MAX_VALUE;
        }

        return 0;
    }

    @Override
    public CommonTree getTree(String file_path, boolean debug) throws Exception {
        if (checkSyntax(file_path, debug) > 0)
            throw new Exception("Errors during parsing");

        return tree;
    }

    private CalculatorLexer getLexer(String file_path) throws IOException {
        return new CalculatorLexer(new ANTLRFileStream(file_path));
    }


}
