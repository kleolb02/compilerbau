package at.tugraz.ist.cc;

import org.antlr.runtime.BaseRecognizer;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.Token;
import org.antlr.runtime.tree.Tree;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class LoggingHelpers {

    public static String traverseTree(Tree tree) {
        String outMsg = "";
        if (tree == null) {
            System.err.println("Could not build a tree. ");
            return "";
        } else {
            for (int i = 0; i < tree.getChildCount(); i++) {
                outMsg += traverseTree(tree.getChild(i), 1);
            }
        }
        return outMsg;
    }

    public static String traverseTree(Tree tree, int level) {
        String msg = "";
        for (int i = 0; i < level; i++) {
            System.out.print("-- ");
            msg = "-- ";
        }

        System.out.println(tree.getText() + " (" + CalculatorParser.tokenNames[tree.getType()] + ")");
        msg += tree.getText() + " (" + CalculatorParser.tokenNames[tree.getType()] + ")" + "\r\n";
        for (int i = 0; i < tree.getChildCount(); i++) {
            msg += traverseTree(tree.getChild(i), level + 1);
        }
        return msg;
    }

    public static String logTokens(CommonTokenStream tokens) {
        String outMsg = "";
        int line = 0;

        for (Token t : tokens.getTokens()) {
            if (t.getType() == -1)
                break;

            for (; line < t.getLine(); line++) //(line < t.getLine())
            {
                System.out.print("\n" + (line + 1) + ":");
                outMsg += "\n" + (line + 1) + ":";
            }

            if (CalculatorParser.tokenNames[t.getType()] != "WS") {
                System.out.print(" " + CalculatorParser.tokenNames[t.getType()]);
                outMsg += " " + CalculatorParser.tokenNames[t.getType()];
            }
        }
        return outMsg;
    }

    static String printErrors(BaseRecognizer recognizer, List<RecognitionException> errors, String kind) {
        String outmsg = "\nNumber of " + kind + " errors: " + errors.size() + "\r\n";
        System.out.println("\nNumber of " + kind + " errors: " + errors.size());
        if (!errors.isEmpty()) {
            for (int i = 1; i <= errors.size(); i++) {
                RecognitionException ex = errors.get(i - 1);
                String msg = recognizer.getErrorMessage(ex, recognizer.getTokenNames());

                System.out.println("  #" + i +
                                ": line " + ex.line +
                                ":" + ex.charPositionInLine +
                                " " + msg
                );
                outmsg += "  #" + i +
                        ": line " + ex.line +
                        ":" + ex.charPositionInLine +
                        " " + msg + "\r\n";

            }
        }
        return outmsg;
    }

    static String getTreeStr(Tree tree)
    {
        return getTreeStr(tree, "", 0);

    }
    static String getTreeStr(Tree tree, String pre, int layer)
    {
        String retMsg = "";//"LAYER: " + layer + "\r\n";
        retMsg += pre + tree.getText() + "\r\n";
        for (int i = 0; i < tree.getChildCount(); i++) {
            if(tree.getChild(i).getChildCount() > 0)
                retMsg += getTreeStr(tree.getChild(i), pre + "\t", layer+1);
            else
                retMsg += pre + "\t" + tree.getChild(i).getText() + " \t\t--(Typ: " + CalculatorParser.tokenNames[tree.getChild(i).getType()] +  ")\r\n";
        }

        return retMsg;
    }

    static String getAllMethodDeclarations(Tree treeNode) {

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < treeNode.getChildCount(); i++) {
            Tree childNode = treeNode.getChild(i);
            MethodInfo method = getMethodInfoForNode(childNode);
            if (method != null) {
                result.append(method.StartLine).append(": BEGIN ").append(method.toString()).append("\n");
                for (int lineNumber = method.StartLine+1; lineNumber < method.FinishLine; lineNumber++)
                    result.append(lineNumber).append(":").append("\n");
                result.append(method.FinishLine).append(": END ").append(method.toString()).append("\n\n\n");
            }

        }

        return result.toString();
    }

    static MethodInfo getMethodInfoForNode(Tree treeNode) {
        if (treeNode == null)
            return null;


        if (treeNode.getType() == CalculatorParser.METHOD_DECL) {
            MethodInfo info = getMethodInfo(treeNode);
            return info;
        }

        return null;
    }

    static String returnError() {
        System.err.println("Could not build a tree. ");
        return "ERROR";
    }

    static MethodInfo getMethodInfo(Tree tree)
    {
        MethodInfo info = new MethodInfo();
        info.StartLine = tree.getLine();

        Tree childForFinishLine = recursivelyFindLastNode(tree);
        info.FinishLine = childForFinishLine.getLine();

        Tree returnNode = tree.getChild(0);
        info.ReturnType = returnNode.getText();

        Tree methodNameNode = tree.getChild(1);
        info.MethodName = methodNameNode.getText();

        Tree paramsNode = tree.getChild(2);
        for (int i = 0; i < paramsNode.getChildCount(); i++)
        {
            Tree parameterNode = paramsNode.getChild(i);
            info.Parameters.add(getFormalParameterFromNode(parameterNode));
        }

        return info;
    }

    private static Tree recursivelyFindLastNode(Tree node) {
        if (node.getChildCount() == 0)
            return node;

        return recursivelyFindLastNode(node.getChild(node.getChildCount()-1));
    }

    private static FormalParameter getFormalParameterFromNode(Tree parameterNode) {
        Tree type = parameterNode.getChild(0);
        Tree name = parameterNode.getChild(1);
        return new FormalParameter(type.getText(), name.getText());
    }

    static void writeOutFile(CalculatorLexer lex, String data) {
        String[] uri = lex.getSourceName().split("\\.");
        File outputFile = new File(uri[0]+ ".out");


        try {
            FileWriter out = new FileWriter(outputFile);
            out.write(data);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
