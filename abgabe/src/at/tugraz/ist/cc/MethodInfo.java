package at.tugraz.ist.cc;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marc on 28.03.2014.
 */
public class MethodInfo {
    public int StartLine = 0;
    public int FinishLine = 0;
    public String MethodName = "";
    public List<FormalParameter> Parameters = new ArrayList<FormalParameter>();
    public String ReturnType = "";

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder
                .append(ReturnType)
                .append(" ")
                .append(MethodName)
                .append("(");

        for (int i=0; i< Parameters.size(); i++) {
            builder.append(Parameters.get(0).toString());

            if (i != Parameters.size()-1)
                builder.append(", ");

        }

        builder.append(")");
        return builder.toString();
    }

}
