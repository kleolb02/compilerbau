// $ANTLR 3.5 src/Calculator.g 2014-03-28 21:32:15

package at.tugraz.ist.cc;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class CalculatorLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int AND=4;
	public static final int ANDOP=5;
	public static final int ARGS=6;
	public static final int ASSIGNMENT=7;
	public static final int ASSIGNMENT_OR_METHOD_CALL=8;
	public static final int ASSIGNOP=9;
	public static final int BLOCK=10;
	public static final int BOOL=11;
	public static final int CASE=12;
	public static final int DIGIT=13;
	public static final int DIGIT0=14;
	public static final int DOWHILE=15;
	public static final int DT_BOOLEAN=16;
	public static final int DT_FLOAT=17;
	public static final int DT_INT=18;
	public static final int DT_STRING=19;
	public static final int ESC_SEQ=20;
	public static final int EXPRESSION=21;
	public static final int FLOAT=22;
	public static final int FOR=23;
	public static final int ID=24;
	public static final int IF=25;
	public static final int INT=26;
	public static final int LCURL=27;
	public static final int LETTER=28;
	public static final int METHOD_BODY=29;
	public static final int METHOD_CALL=30;
	public static final int METHOD_DECL=31;
	public static final int MULTIPLYOP=32;
	public static final int MULTI_LINE_COMMENT=33;
	public static final int NOT=34;
	public static final int OR=35;
	public static final int OROP=36;
	public static final int PARAMETER=37;
	public static final int PARAMS=38;
	public static final int PLUSMINUS=39;
	public static final int PROGRAM=40;
	public static final int RCURL=41;
	public static final int RELOP=42;
	public static final int RETURN=43;
	public static final int SINGLE_LINE_COMMENT=44;
	public static final int STRING=45;
	public static final int VARIABLE=46;
	public static final int VARIABLE_DEF=47;
	public static final int WHILE=48;
	public static final int WS=49;

	private List<RecognitionException> errors = new ArrayList<RecognitionException>();

	public List<RecognitionException> getAllErrors() {
		return errors;
	}

	//override method
	@Override
	public void reportError(RecognitionException e) {
		errors.add(e);
	}



	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public CalculatorLexer() {} 
	public CalculatorLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public CalculatorLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "src/Calculator.g"; }

	// $ANTLR start "T__50"
	public final void mT__50() throws RecognitionException {
		try {
			int _type = T__50;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:20:7: ( '(' )
			// src/Calculator.g:20:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__50"

	// $ANTLR start "T__51"
	public final void mT__51() throws RecognitionException {
		try {
			int _type = T__51;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:21:7: ( ')' )
			// src/Calculator.g:21:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__51"

	// $ANTLR start "T__52"
	public final void mT__52() throws RecognitionException {
		try {
			int _type = T__52;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:22:7: ( ',' )
			// src/Calculator.g:22:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__52"

	// $ANTLR start "T__53"
	public final void mT__53() throws RecognitionException {
		try {
			int _type = T__53;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:23:7: ( ';' )
			// src/Calculator.g:23:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__53"

	// $ANTLR start "T__54"
	public final void mT__54() throws RecognitionException {
		try {
			int _type = T__54;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:24:7: ( 'break' )
			// src/Calculator.g:24:9: 'break'
			{
			match("break"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__54"

	// $ANTLR start "T__55"
	public final void mT__55() throws RecognitionException {
		try {
			int _type = T__55;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:25:7: ( 'continue' )
			// src/Calculator.g:25:9: 'continue'
			{
			match("continue"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__55"

	// $ANTLR start "T__56"
	public final void mT__56() throws RecognitionException {
		try {
			int _type = T__56;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:26:7: ( 'do' )
			// src/Calculator.g:26:9: 'do'
			{
			match("do"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__56"

	// $ANTLR start "T__57"
	public final void mT__57() throws RecognitionException {
		try {
			int _type = T__57;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:27:7: ( 'else' )
			// src/Calculator.g:27:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__57"

	// $ANTLR start "T__58"
	public final void mT__58() throws RecognitionException {
		try {
			int _type = T__58;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:28:7: ( 'return' )
			// src/Calculator.g:28:9: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__58"

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:29:7: ( 'while' )
			// src/Calculator.g:29:9: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "MULTI_LINE_COMMENT"
	public final void mMULTI_LINE_COMMENT() throws RecognitionException {
		try {
			int _type = MULTI_LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:78:2: ( '/*' ( options {greedy=false; } : ( . )* ) '*/' )
			// src/Calculator.g:78:4: '/*' ( options {greedy=false; } : ( . )* ) '*/'
			{
			match("/*"); 

			// src/Calculator.g:78:9: ( options {greedy=false; } : ( . )* )
			// src/Calculator.g:78:39: ( . )*
			{
			// src/Calculator.g:78:39: ( . )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( (LA1_0=='*') ) {
					int LA1_1 = input.LA(2);
					if ( (LA1_1=='/') ) {
						alt1=2;
					}
					else if ( ((LA1_1 >= '\u0000' && LA1_1 <= '.')||(LA1_1 >= '0' && LA1_1 <= '\uFFFF')) ) {
						alt1=1;
					}

				}
				else if ( ((LA1_0 >= '\u0000' && LA1_0 <= ')')||(LA1_0 >= '+' && LA1_0 <= '\uFFFF')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// src/Calculator.g:78:39: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop1;
				}
			}

			}

			match("*/"); 

			 _channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTI_LINE_COMMENT"

	// $ANTLR start "SINGLE_LINE_COMMENT"
	public final void mSINGLE_LINE_COMMENT() throws RecognitionException {
		try {
			int _type = SINGLE_LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:85:2: ( '//' (~ ( '\\r' | '\\n' ) )* ( '\\r' | '\\n' )* )
			// src/Calculator.g:85:4: '//' (~ ( '\\r' | '\\n' ) )* ( '\\r' | '\\n' )*
			{
			match("//"); 

			// src/Calculator.g:85:9: (~ ( '\\r' | '\\n' ) )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '\u0000' && LA2_0 <= '\t')||(LA2_0 >= '\u000B' && LA2_0 <= '\f')||(LA2_0 >= '\u000E' && LA2_0 <= '\uFFFF')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// src/Calculator.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop2;
				}
			}

			// src/Calculator.g:85:23: ( '\\r' | '\\n' )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0=='\n'||LA3_0=='\r') ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// src/Calculator.g:
					{
					if ( input.LA(1)=='\n'||input.LA(1)=='\r' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop3;
				}
			}

			 _channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SINGLE_LINE_COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:91:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
			// src/Calculator.g:91:9: ( ' ' | '\\t' | '\\r' | '\\n' )
			{
			if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			_channel=HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "ASSIGNOP"
	public final void mASSIGNOP() throws RecognitionException {
		try {
			int _type = ASSIGNOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:99:17: ( '=' )
			// src/Calculator.g:99:19: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ASSIGNOP"

	// $ANTLR start "ANDOP"
	public final void mANDOP() throws RecognitionException {
		try {
			int _type = ANDOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:100:17: ( '&&' )
			// src/Calculator.g:100:19: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ANDOP"

	// $ANTLR start "OROP"
	public final void mOROP() throws RecognitionException {
		try {
			int _type = OROP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:101:17: ( '||' )
			// src/Calculator.g:101:19: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "OROP"

	// $ANTLR start "RELOP"
	public final void mRELOP() throws RecognitionException {
		try {
			int _type = RELOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:102:17: ( ( '<' | '<=' | '>' | '>=' | '==' | '!=' ) )
			// src/Calculator.g:102:19: ( '<' | '<=' | '>' | '>=' | '==' | '!=' )
			{
			// src/Calculator.g:102:19: ( '<' | '<=' | '>' | '>=' | '==' | '!=' )
			int alt4=6;
			switch ( input.LA(1) ) {
			case '<':
				{
				int LA4_1 = input.LA(2);
				if ( (LA4_1=='=') ) {
					alt4=2;
				}

				else {
					alt4=1;
				}

				}
				break;
			case '>':
				{
				int LA4_2 = input.LA(2);
				if ( (LA4_2=='=') ) {
					alt4=4;
				}

				else {
					alt4=3;
				}

				}
				break;
			case '=':
				{
				alt4=5;
				}
				break;
			case '!':
				{
				alt4=6;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 4, 0, input);
				throw nvae;
			}
			switch (alt4) {
				case 1 :
					// src/Calculator.g:102:20: '<'
					{
					match('<'); 
					}
					break;
				case 2 :
					// src/Calculator.g:102:24: '<='
					{
					match("<="); 

					}
					break;
				case 3 :
					// src/Calculator.g:102:29: '>'
					{
					match('>'); 
					}
					break;
				case 4 :
					// src/Calculator.g:102:33: '>='
					{
					match(">="); 

					}
					break;
				case 5 :
					// src/Calculator.g:102:38: '=='
					{
					match("=="); 

					}
					break;
				case 6 :
					// src/Calculator.g:102:43: '!='
					{
					match("!="); 

					}
					break;

			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RELOP"

	// $ANTLR start "PLUSMINUS"
	public final void mPLUSMINUS() throws RecognitionException {
		try {
			int _type = PLUSMINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:103:15: ( ( '+' | '-' ) )
			// src/Calculator.g:
			{
			if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUSMINUS"

	// $ANTLR start "MULTIPLYOP"
	public final void mMULTIPLYOP() throws RecognitionException {
		try {
			int _type = MULTIPLYOP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:104:22: ( ( '/' | '%' | '*' ) )
			// src/Calculator.g:
			{
			if ( input.LA(1)=='%'||input.LA(1)=='*'||input.LA(1)=='/' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MULTIPLYOP"

	// $ANTLR start "NOT"
	public final void mNOT() throws RecognitionException {
		try {
			int _type = NOT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:105:17: ( '!' )
			// src/Calculator.g:105:19: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NOT"

	// $ANTLR start "DT_INT"
	public final void mDT_INT() throws RecognitionException {
		try {
			int _type = DT_INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:108:17: ( 'int' )
			// src/Calculator.g:108:19: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DT_INT"

	// $ANTLR start "DT_BOOLEAN"
	public final void mDT_BOOLEAN() throws RecognitionException {
		try {
			int _type = DT_BOOLEAN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:109:17: ( 'boolean' )
			// src/Calculator.g:109:19: 'boolean'
			{
			match("boolean"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DT_BOOLEAN"

	// $ANTLR start "DT_STRING"
	public final void mDT_STRING() throws RecognitionException {
		try {
			int _type = DT_STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:110:16: ( 'String' )
			// src/Calculator.g:110:18: 'String'
			{
			match("String"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DT_STRING"

	// $ANTLR start "DT_FLOAT"
	public final void mDT_FLOAT() throws RecognitionException {
		try {
			int _type = DT_FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:111:15: ( 'float' )
			// src/Calculator.g:111:17: 'float'
			{
			match("float"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DT_FLOAT"

	// $ANTLR start "IF"
	public final void mIF() throws RecognitionException {
		try {
			int _type = IF;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:113:17: ( 'if' )
			// src/Calculator.g:113:19: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IF"

	// $ANTLR start "FOR"
	public final void mFOR() throws RecognitionException {
		try {
			int _type = FOR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:114:18: ( 'for' )
			// src/Calculator.g:114:20: 'for'
			{
			match("for"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FOR"

	// $ANTLR start "LCURL"
	public final void mLCURL() throws RecognitionException {
		try {
			int _type = LCURL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:115:17: ( '{' )
			// src/Calculator.g:115:19: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LCURL"

	// $ANTLR start "RCURL"
	public final void mRCURL() throws RecognitionException {
		try {
			int _type = RCURL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:116:17: ( '}' )
			// src/Calculator.g:116:19: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RCURL"

	// $ANTLR start "BOOL"
	public final void mBOOL() throws RecognitionException {
		try {
			int _type = BOOL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:119:5: ( 'true' | 'false' )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0=='t') ) {
				alt5=1;
			}
			else if ( (LA5_0=='f') ) {
				alt5=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// src/Calculator.g:119:7: 'true'
					{
					match("true"); 

					}
					break;
				case 2 :
					// src/Calculator.g:119:14: 'false'
					{
					match("false"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BOOL"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:123:5: ( '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"' )
			// src/Calculator.g:123:8: '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"'
			{
			match('\"'); 
			// src/Calculator.g:123:12: ( ESC_SEQ |~ ( '\\\\' | '\"' ) )*
			loop6:
			while (true) {
				int alt6=3;
				int LA6_0 = input.LA(1);
				if ( (LA6_0=='\\') ) {
					alt6=1;
				}
				else if ( ((LA6_0 >= '\u0000' && LA6_0 <= '!')||(LA6_0 >= '#' && LA6_0 <= '[')||(LA6_0 >= ']' && LA6_0 <= '\uFFFF')) ) {
					alt6=2;
				}

				switch (alt6) {
				case 1 :
					// src/Calculator.g:123:14: ESC_SEQ
					{
					mESC_SEQ(); 

					}
					break;
				case 2 :
					// src/Calculator.g:123:24: ~ ( '\\\\' | '\"' )
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop6;
				}
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "ESC_SEQ"
	public final void mESC_SEQ() throws RecognitionException {
		try {
			// src/Calculator.g:130:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) )
			// src/Calculator.g:130:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
			{
			match('\\'); 
			if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ESC_SEQ"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:132:5: ( ( LETTER ) ( LETTER | DIGIT0 | '_' )* )
			// src/Calculator.g:132:7: ( LETTER ) ( LETTER | DIGIT0 | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// src/Calculator.g:132:16: ( LETTER | DIGIT0 | '_' )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( ((LA7_0 >= '0' && LA7_0 <= '9')||(LA7_0 >= 'A' && LA7_0 <= 'Z')||LA7_0=='_'||(LA7_0 >= 'a' && LA7_0 <= 'z')) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// src/Calculator.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop7;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "LETTER"
	public final void mLETTER() throws RecognitionException {
		try {
			// src/Calculator.g:138:5: ( 'a' .. 'z' | 'A' .. 'Z' )
			// src/Calculator.g:
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LETTER"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:140:7: ( '0' | DIGIT ( DIGIT0 )* )
			int alt9=2;
			int LA9_0 = input.LA(1);
			if ( (LA9_0=='0') ) {
				alt9=1;
			}
			else if ( ((LA9_0 >= '1' && LA9_0 <= '9')) ) {
				alt9=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// src/Calculator.g:140:10: '0'
					{
					match('0'); 
					}
					break;
				case 2 :
					// src/Calculator.g:140:16: DIGIT ( DIGIT0 )*
					{
					mDIGIT(); 

					// src/Calculator.g:140:22: ( DIGIT0 )*
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( ((LA8_0 >= '0' && LA8_0 <= '9')) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// src/Calculator.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							break loop8;
						}
					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "DIGIT0"
	public final void mDIGIT0() throws RecognitionException {
		try {
			// src/Calculator.g:143:17: ( '0' .. '9' )
			// src/Calculator.g:
			{
			if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT0"

	// $ANTLR start "DIGIT"
	public final void mDIGIT() throws RecognitionException {
		try {
			// src/Calculator.g:144:17: ( '1' .. '9' )
			// src/Calculator.g:
			{
			if ( (input.LA(1) >= '1' && input.LA(1) <= '9') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			}

		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIGIT"

	// $ANTLR start "FLOAT"
	public final void mFLOAT() throws RecognitionException {
		try {
			int _type = FLOAT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// src/Calculator.g:146:8: ( INT '.' ( DIGIT0 )+ )
			// src/Calculator.g:146:12: INT '.' ( DIGIT0 )+
			{
			mINT(); 

			match('.'); 
			// src/Calculator.g:146:20: ( DIGIT0 )+
			int cnt10=0;
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( ((LA10_0 >= '0' && LA10_0 <= '9')) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// src/Calculator.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt10 >= 1 ) break loop10;
					EarlyExitException eee = new EarlyExitException(10, input);
					throw eee;
				}
				cnt10++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FLOAT"

	@Override
	public void mTokens() throws RecognitionException {
		// src/Calculator.g:1:8: ( T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | MULTI_LINE_COMMENT | SINGLE_LINE_COMMENT | WS | ASSIGNOP | ANDOP | OROP | RELOP | PLUSMINUS | MULTIPLYOP | NOT | DT_INT | DT_BOOLEAN | DT_STRING | DT_FLOAT | IF | FOR | LCURL | RCURL | BOOL | STRING | ID | INT | FLOAT )
		int alt11=33;
		alt11 = dfa11.predict(input);
		switch (alt11) {
			case 1 :
				// src/Calculator.g:1:10: T__50
				{
				mT__50(); 

				}
				break;
			case 2 :
				// src/Calculator.g:1:16: T__51
				{
				mT__51(); 

				}
				break;
			case 3 :
				// src/Calculator.g:1:22: T__52
				{
				mT__52(); 

				}
				break;
			case 4 :
				// src/Calculator.g:1:28: T__53
				{
				mT__53(); 

				}
				break;
			case 5 :
				// src/Calculator.g:1:34: T__54
				{
				mT__54(); 

				}
				break;
			case 6 :
				// src/Calculator.g:1:40: T__55
				{
				mT__55(); 

				}
				break;
			case 7 :
				// src/Calculator.g:1:46: T__56
				{
				mT__56(); 

				}
				break;
			case 8 :
				// src/Calculator.g:1:52: T__57
				{
				mT__57(); 

				}
				break;
			case 9 :
				// src/Calculator.g:1:58: T__58
				{
				mT__58(); 

				}
				break;
			case 10 :
				// src/Calculator.g:1:64: T__59
				{
				mT__59(); 

				}
				break;
			case 11 :
				// src/Calculator.g:1:70: MULTI_LINE_COMMENT
				{
				mMULTI_LINE_COMMENT(); 

				}
				break;
			case 12 :
				// src/Calculator.g:1:89: SINGLE_LINE_COMMENT
				{
				mSINGLE_LINE_COMMENT(); 

				}
				break;
			case 13 :
				// src/Calculator.g:1:109: WS
				{
				mWS(); 

				}
				break;
			case 14 :
				// src/Calculator.g:1:112: ASSIGNOP
				{
				mASSIGNOP(); 

				}
				break;
			case 15 :
				// src/Calculator.g:1:121: ANDOP
				{
				mANDOP(); 

				}
				break;
			case 16 :
				// src/Calculator.g:1:127: OROP
				{
				mOROP(); 

				}
				break;
			case 17 :
				// src/Calculator.g:1:132: RELOP
				{
				mRELOP(); 

				}
				break;
			case 18 :
				// src/Calculator.g:1:138: PLUSMINUS
				{
				mPLUSMINUS(); 

				}
				break;
			case 19 :
				// src/Calculator.g:1:148: MULTIPLYOP
				{
				mMULTIPLYOP(); 

				}
				break;
			case 20 :
				// src/Calculator.g:1:159: NOT
				{
				mNOT(); 

				}
				break;
			case 21 :
				// src/Calculator.g:1:163: DT_INT
				{
				mDT_INT(); 

				}
				break;
			case 22 :
				// src/Calculator.g:1:170: DT_BOOLEAN
				{
				mDT_BOOLEAN(); 

				}
				break;
			case 23 :
				// src/Calculator.g:1:181: DT_STRING
				{
				mDT_STRING(); 

				}
				break;
			case 24 :
				// src/Calculator.g:1:191: DT_FLOAT
				{
				mDT_FLOAT(); 

				}
				break;
			case 25 :
				// src/Calculator.g:1:200: IF
				{
				mIF(); 

				}
				break;
			case 26 :
				// src/Calculator.g:1:203: FOR
				{
				mFOR(); 

				}
				break;
			case 27 :
				// src/Calculator.g:1:207: LCURL
				{
				mLCURL(); 

				}
				break;
			case 28 :
				// src/Calculator.g:1:213: RCURL
				{
				mRCURL(); 

				}
				break;
			case 29 :
				// src/Calculator.g:1:219: BOOL
				{
				mBOOL(); 

				}
				break;
			case 30 :
				// src/Calculator.g:1:224: STRING
				{
				mSTRING(); 

				}
				break;
			case 31 :
				// src/Calculator.g:1:231: ID
				{
				mID(); 

				}
				break;
			case 32 :
				// src/Calculator.g:1:234: INT
				{
				mINT(); 

				}
				break;
			case 33 :
				// src/Calculator.g:1:238: FLOAT
				{
				mFLOAT(); 

				}
				break;

		}
	}


	protected DFA11 dfa11 = new DFA11(this);
	static final String DFA11_eotS =
		"\5\uffff\6\33\1\23\1\uffff\1\47\3\uffff\1\50\2\uffff\3\33\2\uffff\1\33"+
		"\2\uffff\2\60\3\33\1\66\3\33\4\uffff\1\33\1\73\5\33\2\uffff\1\60\3\33"+
		"\1\uffff\3\33\1\107\1\uffff\2\33\1\112\5\33\1\120\2\33\1\uffff\2\33\1"+
		"\uffff\1\33\1\126\1\127\2\33\1\uffff\1\33\1\133\1\33\1\135\1\126\2\uffff"+
		"\2\33\1\140\1\uffff\1\141\1\uffff\1\142\1\33\3\uffff\1\144\1\uffff";
	static final String DFA11_eofS =
		"\145\uffff";
	static final String DFA11_minS =
		"\1\11\4\uffff\3\157\1\154\1\145\1\150\1\52\1\uffff\1\75\3\uffff\1\75\2"+
		"\uffff\1\146\1\164\1\141\2\uffff\1\162\2\uffff\2\56\1\145\1\157\1\156"+
		"\1\60\1\163\1\164\1\151\4\uffff\1\164\1\60\1\162\1\157\1\162\1\154\1\165"+
		"\2\uffff\1\56\1\141\1\154\1\164\1\uffff\1\145\1\165\1\154\1\60\1\uffff"+
		"\1\151\1\141\1\60\1\163\1\145\1\153\1\145\1\151\1\60\1\162\1\145\1\uffff"+
		"\1\156\1\164\1\uffff\1\145\2\60\1\141\1\156\1\uffff\1\156\1\60\1\147\2"+
		"\60\2\uffff\1\156\1\165\1\60\1\uffff\1\60\1\uffff\1\60\1\145\3\uffff\1"+
		"\60\1\uffff";
	static final String DFA11_maxS =
		"\1\175\4\uffff\1\162\2\157\1\154\1\145\1\150\1\57\1\uffff\1\75\3\uffff"+
		"\1\75\2\uffff\1\156\1\164\1\157\2\uffff\1\162\2\uffff\1\56\1\71\1\145"+
		"\1\157\1\156\1\172\1\163\1\164\1\151\4\uffff\1\164\1\172\1\162\1\157\1"+
		"\162\1\154\1\165\2\uffff\1\71\1\141\1\154\1\164\1\uffff\1\145\1\165\1"+
		"\154\1\172\1\uffff\1\151\1\141\1\172\1\163\1\145\1\153\1\145\1\151\1\172"+
		"\1\162\1\145\1\uffff\1\156\1\164\1\uffff\1\145\2\172\1\141\1\156\1\uffff"+
		"\1\156\1\172\1\147\2\172\2\uffff\1\156\1\165\1\172\1\uffff\1\172\1\uffff"+
		"\1\172\1\145\3\uffff\1\172\1\uffff";
	static final String DFA11_acceptS =
		"\1\uffff\1\1\1\2\1\3\1\4\7\uffff\1\15\1\uffff\1\17\1\20\1\21\1\uffff\1"+
		"\22\1\23\3\uffff\1\33\1\34\1\uffff\1\36\1\37\11\uffff\1\13\1\14\1\16\1"+
		"\24\7\uffff\1\40\1\41\4\uffff\1\7\4\uffff\1\31\13\uffff\1\25\2\uffff\1"+
		"\32\5\uffff\1\10\5\uffff\1\35\1\5\3\uffff\1\12\1\uffff\1\30\2\uffff\1"+
		"\11\1\27\1\26\1\uffff\1\6";
	static final String DFA11_specialS =
		"\145\uffff}>";
	static final String[] DFA11_transitionS = {
			"\2\14\2\uffff\1\14\22\uffff\1\14\1\21\1\32\2\uffff\1\23\1\16\1\uffff"+
			"\1\1\1\2\1\23\1\22\1\3\1\22\1\uffff\1\13\1\34\11\35\1\uffff\1\4\1\20"+
			"\1\15\1\20\2\uffff\22\33\1\25\7\33\6\uffff\1\33\1\5\1\6\1\7\1\10\1\26"+
			"\2\33\1\24\10\33\1\11\1\33\1\31\2\33\1\12\3\33\1\27\1\17\1\30",
			"",
			"",
			"",
			"",
			"\1\37\2\uffff\1\36",
			"\1\40",
			"\1\41",
			"\1\42",
			"\1\43",
			"\1\44",
			"\1\45\4\uffff\1\46",
			"",
			"\1\20",
			"",
			"",
			"",
			"\1\20",
			"",
			"",
			"\1\52\7\uffff\1\51",
			"\1\53",
			"\1\56\12\uffff\1\54\2\uffff\1\55",
			"",
			"",
			"\1\57",
			"",
			"",
			"\1\61",
			"\1\61\1\uffff\12\62",
			"\1\63",
			"\1\64",
			"\1\65",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\67",
			"\1\70",
			"\1\71",
			"",
			"",
			"",
			"",
			"\1\72",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\74",
			"\1\75",
			"\1\76",
			"\1\77",
			"\1\100",
			"",
			"",
			"\1\61\1\uffff\12\62",
			"\1\101",
			"\1\102",
			"\1\103",
			"",
			"\1\104",
			"\1\105",
			"\1\106",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\1\110",
			"\1\111",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\113",
			"\1\114",
			"\1\115",
			"\1\116",
			"\1\117",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\121",
			"\1\122",
			"",
			"\1\123",
			"\1\124",
			"",
			"\1\125",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\130",
			"\1\131",
			"",
			"\1\132",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\134",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"",
			"\1\136",
			"\1\137",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			"\1\143",
			"",
			"",
			"",
			"\12\33\7\uffff\32\33\4\uffff\1\33\1\uffff\32\33",
			""
	};

	static final short[] DFA11_eot = DFA.unpackEncodedString(DFA11_eotS);
	static final short[] DFA11_eof = DFA.unpackEncodedString(DFA11_eofS);
	static final char[] DFA11_min = DFA.unpackEncodedStringToUnsignedChars(DFA11_minS);
	static final char[] DFA11_max = DFA.unpackEncodedStringToUnsignedChars(DFA11_maxS);
	static final short[] DFA11_accept = DFA.unpackEncodedString(DFA11_acceptS);
	static final short[] DFA11_special = DFA.unpackEncodedString(DFA11_specialS);
	static final short[][] DFA11_transition;

	static {
		int numStates = DFA11_transitionS.length;
		DFA11_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA11_transition[i] = DFA.unpackEncodedString(DFA11_transitionS[i]);
		}
	}

	protected class DFA11 extends DFA {

		public DFA11(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 11;
			this.eot = DFA11_eot;
			this.eof = DFA11_eof;
			this.min = DFA11_min;
			this.max = DFA11_max;
			this.accept = DFA11_accept;
			this.special = DFA11_special;
			this.transition = DFA11_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | MULTI_LINE_COMMENT | SINGLE_LINE_COMMENT | WS | ASSIGNOP | ANDOP | OROP | RELOP | PLUSMINUS | MULTIPLYOP | NOT | DT_INT | DT_BOOLEAN | DT_STRING | DT_FLOAT | IF | FOR | LCURL | RCURL | BOOL | STRING | ID | INT | FLOAT );";
		}
	}

}
