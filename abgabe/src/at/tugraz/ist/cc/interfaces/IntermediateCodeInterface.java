package at.tugraz.ist.cc.interfaces;

import java.io.OutputStream;

public interface IntermediateCodeInterface {
    
    public int createIntermediateCode(String file_path, boolean debug, OutputStream out);
    
}
