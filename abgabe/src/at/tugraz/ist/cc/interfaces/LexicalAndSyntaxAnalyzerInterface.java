package at.tugraz.ist.cc.interfaces;

import org.antlr.runtime.tree.CommonTree;

public interface LexicalAndSyntaxAnalyzerInterface {

    public int lexer(String file_path, boolean debug);
    
    public int checkSyntax(String file_path, boolean debug);

    CommonTree getTree(String file_path, boolean debug) throws Exception;
}
