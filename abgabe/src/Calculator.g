grammar Calculator;

options{
backtrack = false;
k = 1;
output = AST;
ASTLabelType=CommonTree;
}


tokens {
METHOD_DECL;
PARAMETER;
PROGRAM;
EXPRESSION;
ASSIGNMENT;
METHOD_CALL;
ASSIGNMENT_OR_METHOD_CALL;
OR;
AND;
NOT;
VARIABLE;
METHOD_BODY;
VARIABLE_DEF;
ARGS;
BLOCK;
DOWHILE;
WHILE;
RETURN;
CASE;
PARAMS;
}

@lexer::header{
package at.tugraz.ist.cc;
}

@lexer::members
{
private List<RecognitionException> errors = new ArrayList<RecognitionException>();

public List<RecognitionException> getAllErrors() {
	return errors;
}

//override method
@Override
public void reportError(RecognitionException e) {
	errors.add(e);
}

}

@parser::header{
package at.tugraz.ist.cc;
}
@parser::members
{
private List<RecognitionException> errors = new ArrayList<RecognitionException>();

public List<RecognitionException> getAllErrors() {
	return errors;
}

//override method

@Override
public void reportError(RecognitionException e) {
	errors.add(e);
}

}




MULTI_LINE_COMMENT
	: '/*' ( options { greedy=false; } : .*) '*/'
	{ $channel=HIDDEN;}
	; 



SINGLE_LINE_COMMENT
	: '//' ~('\r'|'\n')* ('\r'|'\n')*
	{ $channel=HIDDEN;}
	; 



WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;
    
//Operators
ASSIGNOP        : '=';
ANDOP           : '&&';
OROP            : '||';
RELOP           : ('<'|'<='|'>'|'>='|'=='|'!=');
PLUSMINUS	    :	('+'|'-');
MULTIPLYOP           : ('/'|'%'|'*');
NOT             : '!';

//Data Types
DT_INT          : 'int';
DT_BOOLEAN      : 'boolean';
DT_STRING      : 'String';
DT_FLOAT      : 'float';

IF              : 'if';
FOR              : 'for';
LCURL           : '{';
RCURL           : '}';

BOOL
    : 'true'|'false'
    ;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;


fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    ;

ID  :	(LETTER) (LETTER|DIGIT0|'_')*
    ;

fragment
LETTER
    :	'a'..'z'|'A'..'Z'
    ;

INT  	:	 '0' | DIGIT (DIGIT0)*;
//INT  	:	 '0' | DIGIT (DIGIT0)* { checkThatIsValidNumberSuffix(); } ;

fragment DIGIT0 : '0'..'9';
fragment DIGIT  : '1'..'9';

FLOAT 	:   INT '.' DIGIT0+ ;


program :
	methodDeclarations -> ^(PROGRAM methodDeclarations)
	;

methodDeclarations
:
	(methodDeclaration)+
;

methodDeclaration
	:
    methodHeader
    methodBody -> ^(METHOD_DECL methodHeader methodBody)
	;

methodHeader
    :
    primitiveType
    ID
    formalParameterList
    ;

methodBody
    :
    	LCURL
    	(blockStatement)*
    	returnStatement
    	RCURL
    	-> ^(METHOD_BODY (blockStatement)* returnStatement RCURL) //  we need the RCURL for proper line number reporting
    ;
    
   returnStatement
:
'return' expression ';' 
-> ^(RETURN expression)
;

blockStatement
    :   localVariableDeclarationStatement
    |   statement
    ;

statement
	: ifStatement
	| forStatement
	| block
	| whileStatement
    	| 'do'^ statement 'while'! paranthesisExpression ';'!
    	| 'break'^ ';'!
    	| 'continue'^ ';'!
    	| (assignmentOrMethodCallStatement ';'!)
	;

whileStatement
	:
	'while' paranthesisExpression statement
	-> ^(WHILE paranthesisExpression statement)
	;


localVariableDeclarationStatement
    :   localVariableDeclaration^
        ';'!
    ;

ifStatement
	: IF paranthesisExpression statement
	( options { greedy=true;}: elseStatement)?
	-> ^(IF paranthesisExpression statement elseStatement?)
	;

elseStatement
	: 'else'! statement^
	;

forStatement
	:
	FOR^ '('!
	(forInitClause) ';'!
	(expression) ';'!
	(assignmentOrMethodCallStatement)
	')'!
	statement
	;

forInitClause
	: localVariableDeclaration
	| variableAssignment
	;

localVariableDeclaration
	:
	primitiveType
	variableAssignment
	(',' variableAssignment)*
	-> ^(VARIABLE_DEF primitiveType variableAssignment variableAssignment*)
	;

variableAssignment
	:
	ID
	('=' expression)?
	-> ^(VARIABLE ID expression?)
	;

block	:
	LCURL (blockStatement)* RCURL
	-> ^(BLOCK blockStatement*)
	;

formalParameterList
	:
	'('
	(formalParameters)?
	')'
	-> ^(PARAMS (formalParameters)? )
	;

formalParameters
	:
	formalParameter
	(','! formalParameter)*
	;

formalParameter
	:
	primitiveType
	ID -> ^(PARAMETER primitiveType ID)
	;

primitiveType
    :   DT_BOOLEAN
    |   DT_INT
    |   DT_STRING
    |   DT_FLOAT
    ;

paranthesisExpression
	:
	'('
	expression
	')'
	-> ^(EXPRESSION expression)
	;

expression
	:
	orExpression
//	(assignmentOperator expression)?
;

assignmentOrMethodCallStatement
	:
	(ID-> ID)
	( '(' (arguments)? ')'  -> ^(METHOD_CALL $assignmentOrMethodCallStatement ^(ARGS (arguments)?) ) 
	| assignmentOperator expression -> ^(ASSIGNMENT $assignmentOrMethodCallStatement expression)  
	) 
	;

assignmentOperator
	:
	'='
	;

orExpression
	:
	andExpression
	('||'^ andExpression)*
	;
		

andExpression
	: relExpression
	('&&'^ relExpression)*
	;

relExpression
	: addExpression
	(RELOP^ addExpression)*
	;

addExpression
	:
	multiplyExpression
	(PLUSMINUS^ multiplyExpression)*
	;

multiplyExpression
	:
	unaryExpression
	(MULTIPLYOP^ unaryExpression)*
	;

unaryExpression
	: PLUSMINUS notExpression -> ^(PLUSMINUS notExpression)
    	| notExpression
	;

notExpression
	: '!' basicExpression -> ^(NOT basicExpression)
	| basicExpression
	;

basicExpression
	: paranthesisExpression
	| variableAccessOrMethodCall
	| literal
	;

variableAccessOrMethodCall
	:
	ID
	('(' (arguments)? ')')?
	;

arguments
	:
	expression
	(','! expression)*
	;

literal
	: BOOL
	| STRING
	| INT
	| FLOAT
	;
