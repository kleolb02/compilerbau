tree grammar Eval;

options {
    tokenVocab=Calculator;
    output=AST;
    ASTLabelType=CommonTree;
    backtrack=true;
}

@header {
package at.tugraz.ist.cc;
}

@members {
}
